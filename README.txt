Console Sandbox Game
====================


Basic (and advanced) commands:
----------------------------------------
Some commands (eg. moving the cursor, printing inventory contents, or listing items) don't advance the world time.

help - shows a list of all commands
help <n> - shows nth page of a list of all commands

exit - stop the program

w, a, s, d - (these are commands) movement
w <n>, a <n>, s <n>, d <n> - auto-movement by n steps (1 ≤ n ≤ 20)
Note: Auto-movement doesn't work in water
up, down - move up and down ladders

i, j, k, l - move the cursor (the arrangement is like wasd)
-, = - move the cursor layer down or layer up

b <x> = build <x> - build a tile with ID x in the selected location (for now you can place blocks infinitely)
m = mine - mine a tile in the selected location
e = examine - print some into about the selected location (tile, entities, items)
r = hit - punch an entity in the selected location
p = pickup - pick up an item from the ground at the selected location

load - load world from file world.bin
save - save world to file world.bin

tilelist - list all tiles (appearance, ID, name and properties)
itemlist - list all items (appearance, ID and name)
IDs are shown only if setting showIds has value 1.

Empty line, or command `look` prints the map again.

Setting debug = 1 gives access to debug commands:
test - shows information (which I often change while I'm testing various things)
colortest - shows all avaliable colors
randomtp = rtp - teleports to a random location on the map
tp <x> <y> - teleports to a given location on the map
give <id> [count] - gives the player an item
spawn [count] - spawns pigs
debug - toggles debug info
spawn2 [id] - spawns an item with id [id]
killall - kills all entities apart from the player


Interface:
---------

"Screenshot" from the game (turn off line wrap):
(1)
''''''''......~~......~~......................~~................~~..~~~~....~~..~~......~~....................................''""''""''""""""""''''''**""''""''""""""""''**''......
""''""''..........~~........~~..~~~~....~~....~~............~~..~~..~~~~~~~~~~....~~~~~~....................................""""''""''""''''''''""""''""''''''''""""""""''""''......
''**''""''......~~..~~~~........~~......~~~~....................~~~~~~....~~~~..~~~~....~~............""''''''''""''''..''''''''""""""""''""**""''''""""''''''''""''""''""""""......
''""''""''..........~~......~~....~~..~~......~~..~~........~~......~~~~......~~~~~~~~............""""""''""''""''''""""''''''''""''""''""""""""''''""""''""''""''''''''""""""......
""""""""''""............~~..~~~~..~~~~~~..~~......~~~~..~~~~..~~............~~~~~~............''""''""''""""""""''''""''""""""""''''''''""''""''""""''''""""""""''""''""''''........
..''""''""""""............~~~~~~~~~~~~~~~~~~~~..~~~~~~..~~~~......~~~~......~~~~..........ඞ'''''""""""""''""''""''''**""''''''''""''""''**""""""''''""""''""''""''''''''""""........
....''''""''..........~~......~~..~~....~~..~~..~~......~~....~~~~~~..~~..~~..~~........''''''''""""""""''""''""''''""""''**''""''''''''""""""""''""""""''""**""''''''''""''........
..........................~~..~~~~..~~........~~~~..~~..~~~~~~~~~~~~~~..~~..........""""''''''''""''""''""""""""''**""""''""''""''''''''""""""""''""''''""""""""''""''""''........~~
..........................~~....~~....~~..~~..~~~~..~~~~........~~..~~....~~........""''**""""""''''''''""''""''""""''''""""""""''""**""''''''''""''""''""""""""''''''''""......~~~~
....................................~~............~~..~~~~~~~~~~....~~............''""""''''''''""''""''""""""""''''""''""""""""''''''**""''""''""""''''""''""''""""""""............
(2) Selected tile:   Air @ (547, 457, 6) ~ (0, 0, 0) | Entities: 1

(3)
E: 1/1
FPS: 10 MSPF: 91ms

(4)


(5) [T: 0] (547, 457, 6) HP: 50/50 Gracz >

(1) Map - shows the world around you
    The map shows only blocks on the "eye level" and on "floor level".
    If there are blocks below floor level, the map shows air (so you can see holes in the ground).
    Unfortunately however, if you fall in a hole, where the floor and the walls are the same, you can't see walls.
(2) Information about the selection: block name @ (block location) ~ (block location relative to player) | Selected entities count
(3) Debug info (setting debug = 1)
    TPS: <max ticks per second> MSPT: <milliseconds spent to tick the world> (hidden if the world didn't tick)
    E: <entities in world>/<entities in camera>
    FPS: <max frames per second> MSPF: <milliseconds spent to render frame>
(4) Reserved place to show info about result of an action you performed.
(5) Prompt: [T: world time] (player location) HP: health/maxHealth Nickname >


All 3D coordinates have format:
(x, y, layer)
x - column
y - row
layer - height in the world


Settings:
----------

Settings are loaded in order:
 - from default values (hardcoded)
 - from file "settings.txt"
 - from program args
Settings are loaded only at the start of the program. If you want to change the settings, you have to restart the program.

File settings.txt contains lines. Every line has format:
setting = value
and contains one setting.

Command line arguments have format:
--setting value

All of the settings are numbers, settings with type boolean should have numerical values 0 or 1

Most important settings are:
windowWidth - how many tiles horizontal are shown on the map
windowHeight - how many tiles vertical are shown on the map
useColor - enable colors
extraColors - enable extra colors (256 colors instead of the usual 16)
reverseBlackWhite - IntelliJ IDEA's terminal reverses black and white color codes, this setting reverts them back
showIds - show or hite block and item IDs and entity UUIDs

A list of all of the settings is avaliable in tadeln.csg.Settings class


FAQ:
---

Everything is shuffled! I can't see anything!
    Probably your terminal size is too small for the game resolution.
    Change settings windowWidth and windowHeight in settings.txt or in command line arguments.
    windowWidth: width of your terminal in chars (divide by 2 if doubleTileWidth = 1)
    windowHeight: (height of your terminal in lines - value of printLines setting) - 3

I can't mine/place a tile/attack!
    Make sure that your selection is at the correct height level / layer.

How can I go up a layer?
    Build a ladder using the command `b 9`, go in the place of the ladder, use the `up` command to go up the ladder.


Author:
-----

TadeLn
https://tadeln.stary.pc.pl/
https://gitlab.com/TadeLn/csg

11.2020