Konsolowa Gra Sandbox
=====================


Podstawowe (i ponadpodstawowe) polecenia:
----------------------------------------
Niektóre polecenia (np. przesuwanie kursora, wyświetlanie ekwipunku, czy list przedmiotów) nie tickują świata.

help - wyświetla listę wszytkich poleceń
help <n> - wyświetla stronę n poleceń

exit - zakończ program

w, a, s, d - (to są polecenia) poruszanie się
w <n>, a <n>, s <n>, d <n> - auto-poruszanie się o n kroków (1 ≤ n ≤ 20)
Uwaga: Auto-poruszanie się nie działa w wodzie
up, down - polecenia do poruszania się w górę i w dół po drabinie

i, j, k, l - poruszanie kursorem (ułożenie jak wasd)
-, = - poruszanie kursorem poziom w dół lub poziom w górę

b <x> = build <x> - zbuduj blok o ID x w zaznaczonym miejscu (jak na razie można wstawiać nieskończoną ilość bloków)
m = mine - wykop blok w zaznaczonym miejscu
e = examine - pokaż informacje o zaznaczonym miejscu (blok, byty, leżące przedmioty)
r = hit - uderz przeciwnika w zaznaczonym miejscu
p = pickup - podnieś przedmiot w zaznaczonym miejscu

load - wczytaj świat z pliku world.bin
save - zapisz świat do pliku world.bin

tilelist - pokaż wszystkie bloki (ich wygląd, ID, nazwę i ich ogólne właściwości)
itemlist - pokaż wszystkie przedmioty (ich wygląd, ID i nazwę)
ID pokazuje się gdy ustawienie showIds ma wartość 1.

Pusta komenda, lub komenda `look` wyświetla mapę.

Ustawienie debug = 1 daje dostęp do komend do debugowania:
test - pokazuje informację (którą często zmieniam, testuję różne rzeczy)
colortest - pokazuje wszystkie dostępne kolory
randomtp = rtp - teleportuje w losowe miejsce na mapie
tp <x> <y> - teleportuje w wybrane miejsce na mapie
give <id> [ilość] - daj przedmiot do ekwipunku
spawn [ilość] - sprawi że pojawią się świnie
debug - włącza/wyłącza informacje o debugowaniu
spawn2 [id] - sprawi że przedmiot z id [id] pojawi się blisko gracza
killall - zabij wszystko oprócz gracza


Interfejs:
---------

"Screenshot" z gry:
(1)
''''''''......~~......~~......................~~................~~..~~~~....~~..~~......~~....................................''""''""''""""""""''''''**""''""''""""""""''**''......
""''""''..........~~........~~..~~~~....~~....~~............~~..~~..~~~~~~~~~~....~~~~~~....................................""""''""''""''''''''""""''""''''''''""""""""''""''......
''**''""''......~~..~~~~........~~......~~~~....................~~~~~~....~~~~..~~~~....~~............""''''''''""''''..''''''''""""""""''""**""''''""""''''''''""''""''""""""......
''""''""''..........~~......~~....~~..~~......~~..~~........~~......~~~~......~~~~~~~~............""""""''""''""''''""""''''''''""''""''""""""""''''""""''""''""''''''''""""""......
""""""""''""............~~..~~~~..~~~~~~..~~......~~~~..~~~~..~~............~~~~~~............''""''""''""""""""''''""''""""""""''''''''""''""''""""''''""""""""''""''""''''........
..''""''""""""............~~~~~~~~~~~~~~~~~~~~..~~~~~~..~~~~......~~~~......~~~~..........ඞ'''''""""""""''""''""''''**""''''''''""''""''**""""""''''""""''""''""''''''''""""........
....''''""''..........~~......~~..~~....~~..~~..~~......~~....~~~~~~..~~..~~..~~........''''''''""""""""''""''""''''""""''**''""''''''''""""""""''""""""''""**""''''''''""''........
..........................~~..~~~~..~~........~~~~..~~..~~~~~~~~~~~~~~..~~..........""""''''''''""''""''""""""""''**""""''""''""''''''''""""""""''""''''""""""""''""''""''........~~
..........................~~....~~....~~..~~..~~~~..~~~~........~~..~~....~~........""''**""""""''''''''""''""''""""''''""""""""''""**""''''''''""''""''""""""""''''''''""......~~~~
....................................~~............~~..~~~~~~~~~~....~~............''""""''''''''""''""''""""""""''''""''""""""""''''''**""''""''""""''''""''""''""""""""............
(2) Selected tile:   Air @ (547, 457, 6) ~ (0, 0, 0) | Entities: 1

(3)
E: 1/1
FPS: 10 MSPF: 91ms

(4)


(5) [T: 0] (547, 457, 6) HP: 50/50 Gracz >

(1) Mapa - pokazuje świat dookoła ciebie
    Na mapie pokazane są tylko bloki na poziomie "oczu gracza" oraz na poziomie "podłogi pod graczem".
    Jeśli bloki są niżej niż poziom podłogi, pokazuje się powietrze (aby było widać dziury w ziemi).
    Niestety jednak, jeśli wpadniesz do dziury, w której podłoga i ściany są identyczne, nie widać ścian dziury.
(2) Informacje o zaznaczeniu: nazwa bloku @ (koordynaty) ~ (koordynaty relatywne do gracza) | Liczba bytów pod kursorem
(3) Informacje do debugowania (ustawienie debug = 1)
    TPS: <maksymalna liczba ticków na sekundę> MSPT: <czas przeznaczony na tick> (nie jest pokazane jeśli tick nie minął)
    E: <bytów na świecie>/<bytów widocznych>
    FPS: <maksymalna liczba klatek na sekundę> MSPF: <czas przeznaczony na renderowanie>
(4) Miejsce na informacje o przed chwilą wykonanej akcji
(5) Prompt: [T: czas świata] (koordynaty gracza) HP: życie/maxŻycieGracza Nickname >


Wszyskie trójwymiarowe koordynaty mają format:
(x, y, warstwa)
x - kolumna
y - wiersz
warstwa - wysokość


Ustawienia:
----------

Ustawienia są odczytywane w kolejności:
 - z wartości domyślnych (hardcoded)
 - z pliku "settings.txt"
 - z argumentów programu
Ustawienia są wczytywane na początku programu. Aby zmienić ustawienia należy zrestartować program

Plik settings.txt zawiera linie. Każda linia ma format
ustawienie = wartość
i zawiera jedno ustawienie.

Argumenty mają format
--ustawienie wartość

Wszystkie ustawienia działają na liczbach, więc wartości typu boolean oznacza się liczbą 0 lub 1

Najważniejsze ustawienia to:
windowWidth - ilość pokazywanych bloków horyzontalnie (ilość znaków jest 2x większa, każdy blok ma 2 znaki szerokości)
windowHeight - ilość pokazywanych bloków wertykalnie
useColor - czy używać jakichkolwiek kolorów
extraColors - czy używać 256 kolorów (normalnie używa tylko 16)
reverseBlackWhite - terminal w IntelliJ IDEA odwraca czarny i biały kolor, to ustawienie odwraca odwrócenie
showIds - czy pokazywać ID bloków, przedmiotów i bytów

Lista wszystkich ustawień jest dostępna w klasie tadeln.csg.Settings


FAQ:
---

Wszystkie bloki są pomieszane! Nic nie widzę!
    Prawdopodobnie rozmiar terminala jest za mały na ustawioną rozdzielczość.
    Zmień ustawienia windowWidth i windowHeight w settings.txt lub w argumentach linii poleceń.
    windowWidth: szerokość twojego terminala w znakach (podziel na 2 jeśli doubleTileWidth = 1)
    windowHeight: (wysokość twojego terminala w liniach - wartość printLines - 3)

Nie mogę zniszczyć/postawić bloku/atakować!
    Sprawdź czy zaznaczenie jest na pewno na odpowiedniej warstwie/wysokości.

Jak mam wejść na wyższy poziom wysokości?
    Zbuduj drabinę używając polecenia `b 9`, wejdź w miejsce drabiny, użyj polecenia `up` aby wejść na górę.


Autor:
-----

TadeLn
https://tadeln.stary.pc.pl/
https://gitlab.com/TadeLn/csg

11.2020