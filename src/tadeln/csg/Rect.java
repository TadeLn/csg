package tadeln.csg;

public class Rect {

    private Vector2 pos;

    private Vector2 size;

    public Vector2 getPos() {
        return pos;
    }

    public void setPos(Vector2 pos) {
        this.pos = pos;
    }

    public Vector2 getSize() {
        return size;
    }

    public void setSize(Vector2 size) {
        this.size = size;
    }

    public Vector2 getCenter() {
        return pos.add(size.divide(2));
    }

    public void setCenter(Vector2 center) {
        pos = center.subtract(size.divide(2));
    }

    public Rect() {
        this(new Vector2(0, 0), new Vector2(0, 0));
    }

    public Rect(Vector2 pos, Vector2 size) {
        this.pos = pos;
        this.size = size;
    }
}
