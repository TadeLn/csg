package tadeln.csg.world;

import tadeln.csg.*;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.entity.Pig;
import tadeln.csg.world.entity.Player;
import tadeln.csg.world.item.Item;
import tadeln.csg.world.item.MeatItem;
import tadeln.csg.world.item.StickItem;
import tadeln.csg.world.item.TileItem;
import tadeln.csg.world.tile.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class World {

    private int age;

    private static List<Tile> tileRegistry;

    private static List<Item> itemRegistry;


    private Vector3 size;

    private byte[][][] tiles;

    private Player player;

    private List<Entity> entities;


    public int getAge() {
        return age;
    }

    public static List<Tile> getTileRegistry() {
        return tileRegistry;
    }

    public static List<Item> getItemRegistry() {
        return itemRegistry;
    }

    public Vector3 getSize() {
        return size;
    }

    public byte[][][] getTiles() {
        return tiles;
    }

    public void setTiles(byte[][][] tiles) {
        this.tiles = tiles;
    }

    public byte getTileId(Vector3 location) {
        if (Utils.isInRange(location.getX(), 0, size.getX()-1) &&
                Utils.isInRange(location.getY(), 0, size.getY()-1) &&
                Utils.isInRange(location.getZ(), 0, size.getZ()-1)
        ) {
            return tiles[location.getZ()][location.getX()][location.getY()];
        } else {
            return 0;
        }
    }

    public byte getTileId(Vector2 location, int layer) {
        if (Utils.isInRange(location.getX(), 0, size.getX()-1) &&
                Utils.isInRange(location.getY(), 0, size.getY()-1) &&
                layer > 0
        ) {
            Vector3 location3d = new Vector3(location);
            location3d.setZ(layer);

            byte tileId = getTileId(location3d);
            while (tileId == 0) {
                tileId = getTileId(location3d);
                location3d.setZ(location3d.getZ()-1);
                if (location3d.getZ() < layer - 2) {
                    return 0;
                }
            }
            return tileId;
        } else {
            return 0;
        }
    }

    public Tile getTile(Vector2 location, int maxLayer) {
        return tileRegistry.get(getTileId(location, maxLayer));
    }

    public Tile getTile(Vector3 location) {
        return tileRegistry.get(getTileId(location));
    }

    public void setTileId(Vector3 location, byte tileId) {
        if (Utils.isInRange(location.getX(), 0, size.getX()) &&
                Utils.isInRange(location.getY(), 0, size.getY()) &&
                Utils.isInRange(location.getZ(), 0, size.getZ())
        ) {
            tiles[location.getZ()][location.getX()][location.getY()] = tileId;
        }
    }

    public void setTile(Vector3 location, Tile tile) {
        setTileId(location, tile.getId());
    }

    public int getHeight(Vector2 location) {
        int result = getSize().getZ();
        while (getTileId(new Vector3(location).setZ(result)) == AirTile.getIdStatic()) {
            result--;
        }
        return result;
    }

    public Player getPlayer() {
        return player;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public Entity getEntityById(int id) {
        for (Entity e : entities) {
            if (e.getUuid() == id) {
                return e;
            }
        }
        return null;
    }

    public List<Entity> getEntitiesAtLocation(Vector3 location) {
        List<Entity> result = new ArrayList<>();
        for (Entity e : entities) {
            Vector3 pos = e.getPosition();
            if (pos.equals(location)) {
                result.add(e);
            }
        }
        return result;
    }

    public List<Entity> getEntitiesAtLocation(Vector2 location) {
        List<Entity> result = new ArrayList<>();
        for (Entity e : entities) {
            Vector3 pos = e.getPosition();
            if (pos.getX() == location.getX() &&
                pos.getY() == location.getY()
            ) {
                result.add(e);
            }
        }
        return result;
    }


    public void spawnEntity(Entity entity) {
        spawnEntity(entity, false);
    }

    public void spawnEntity(Entity entity, boolean forceSpawn) {
        if (entity.onSpawn(this) || forceSpawn) {
            entities.add(entity);
        }
    }

    public void tick() {
        age++;

        // Spawn entities
        if (age % 10 == 1) {
            for (int i = 0; i < 100; i++) {
                Entity e = new Pig();
                Vector2 pos = new Vector2(
                        (int) (Math.random() * getSize().getX()),
                        (int) (Math.random() * getSize().getY())
                );
                e.setPosition(new Vector3(pos).setZ(getHeight(pos) + 1));
                spawnEntity(e);
                if (entities.size() > 1000) break;
            }
        }

        // Tick all entities
        for (int i = 0; i < entities.size(); i++) {
            entities.get(i).tick(this);
        }

        // Check for entities to destroy
        for (int i = 0; i < entities.size(); i++) {
            if (entities.get(i).isDespawned()) {
                entities.remove(i);
                i--;
            }
        }
    }

    public void draw(Rect camera, Vector2 selectedTile) {
        int cameraBoundaryLeft = camera.getPos().getX();
        int cameraBoundaryRight = cameraBoundaryLeft + camera.getSize().getX();
        int cameraBoundaryTop = camera.getPos().getY();
        int cameraBoundaryBottom = cameraBoundaryTop + camera.getSize().getY();

        // Draw the world
        List<Entity> entitiesInCamera = new ArrayList<>();
        for (Entity entity : entities) {
            if (
                    entity.getPosition().getX() >= cameraBoundaryLeft &&
                    entity.getPosition().getX() <= cameraBoundaryRight &&
                    entity.getPosition().getY() >= cameraBoundaryTop &&
                    entity.getPosition().getY() <= cameraBoundaryBottom
            ) {
                entitiesInCamera.add(entity);
            }
        }

        Debug.add("E: " + entities.size() + "/" + entitiesInCamera.size() + "\n");

        for (int y = camera.getPos().getY(); y < camera.getPos().getY() + camera.getSize().getY(); y++) {

            // Entities to draw in this line
            List<Entity> entitiesInLine = new ArrayList<>();
            for (Entity entity : entitiesInCamera) {
                if (entity.getPosition().getY() == y) {
                    entitiesInLine.add(entity);
                }
            }

            for (int x = camera.getPos().getX(); x < camera.getPos().getX() + camera.getSize().getX(); x++) {
                // Get current tile
                Tile tile = getTile(new Vector2(x, y), player.getPosition().getZ());

                Formatting tileFormatting = new Formatting(tile.getFormatting(new Vector2(x, y)));
                char tileChar = tile.getCharacter(new Vector2(x, y));

                boolean entityExists = false;
                Formatting entityFormatting = new Formatting();
                char entityChar = ' ';
                boolean repeatEntityChar = false;

                for (int i = 0; i < entitiesInLine.size(); i++) {
                    Entity entity = entitiesInLine.get(i);
                    if (entity.getPosition().getX() < x) {
                        entitiesInLine.remove(i);
                    }
                    if (entity.getPosition().getX() == x) {
                        entityChar = entity.getCharacter();
                        entityFormatting = new Formatting(entity.getFormatting()).background(tile.getFormatting().getBackground());
                        repeatEntityChar = entity.isRepeatCharacter();
                        entitiesInLine.remove(i);
                        entityExists = true;
                        break;
                    }
                }

                if (selectedTile.getX() == x && selectedTile.getY() == y) {
                    tileFormatting.reverse();
                    entityFormatting.reverse();
                }
                if (entityExists) {
                    if (Settings.doubleTileWidth) {
                        if (repeatEntityChar) {
                            Renderer.print(new FormattedString("" + entityChar + entityChar, entityFormatting));
                        } else {
                            Renderer.print(new FormattedString("" + entityChar, entityFormatting));
                            Renderer.print(new FormattedString("" + tileChar, tileFormatting));
                        }
                    } else {
                        Renderer.print(new FormattedString("" + entityChar, entityFormatting));
                    }
                } else {
                    if (Settings.doubleTileWidth) {
                        Renderer.print(new FormattedString("" + tileChar + tileChar, tileFormatting));
                    } else {
                        Renderer.print(new FormattedString("" + tileChar, tileFormatting));
                    }
                }
            }
            Renderer.print(new FormattedString("\n"));
        }
    }

    public void save() {
        final String filename = "world.bin";
        try {
            OutputStream file = new FileOutputStream(filename);
            file.write(Utils.intToBytes(size.getX()));
            file.write(Utils.intToBytes(size.getY()));
            file.write(Utils.intToBytes(size.getZ()));
            file.write(Utils.intToBytes(player.getPosition().getX()));
            file.write(Utils.intToBytes(player.getPosition().getY()));
            file.write(Utils.intToBytes(player.getPosition().getZ()));
            file.write(Utils.intToBytes(age));
            for (int z = 0; z < size.getZ(); z++) {
                for (int x = 0; x < size.getX(); x++) {
                    file.write(tiles[z][x]);
                }
            }
            file.close();
            Renderer.printOnLoop(new FormattedString("Saved successfully", new Formatting(Color.GREEN)));
        } catch (IOException e) {
            Renderer.printOnLoop(new FormattedString("Couldn't save", new Formatting(Color.RED)));
        }
    }

    public void load() {
        final String filename = "world.bin";
        try {
            InputStream file = new FileInputStream(filename);
            byte[] arr = new byte[4];

            file.read(arr);
            size.setX(Utils.bytesToInt(arr));
            file.read(arr);
            size.setY(Utils.bytesToInt(arr));
            file.read(arr);
            size.setZ(Utils.bytesToInt(arr));

            create(size);

            Vector3 playerPos = new Vector3();
            file.read(arr);
            playerPos.setX(Utils.bytesToInt(arr));
            file.read(arr);
            playerPos.setY(Utils.bytesToInt(arr));
            file.read(arr);
            playerPos.setZ(Utils.bytesToInt(arr));

            file.read(arr);
            age = Utils.bytesToInt(arr);

            for (int z = 0; z < size.getZ(); z++) {
                for (int x = 0; x < size.getX(); x++) {
                    file.read(tiles[z][x]);
                }
            }
            file.close();

            spawnPlayer(Game.getNickname());
            player.setPosition(playerPos);
            Renderer.printOnLoop(new FormattedString("Loaded successfully", new Formatting(Color.GREEN)));
        } catch (IOException e) {
            Renderer.printOnLoop(new FormattedString("Couldn't load", new Formatting(Color.RED)));
        }
    }

    public void spawnPlayer(String nickname) {
        player = new Player(nickname);
        spawnEntity(player);
    }

    public void create(Vector3 size) {
        age = 0;
        this.size = size;

        tiles = new byte[size.getZ()][size.getX()][size.getY()];
        for (int z = 0; z < size.getZ(); z++) {
            for (int x = 0; x < size.getX(); x++) {
                for (int y = 0; y < size.getY(); y++) {
                    tiles[z][x][y] = 0;
                }
            }
        }

        entities = new ArrayList<>();
    }

    public World(Vector3 size) {
        tileRegistry = new ArrayList<>();
        tileRegistry.add(new AirTile((byte) tileRegistry.size()));
        tileRegistry.add(new GrassTile((byte) tileRegistry.size()));
        tileRegistry.add(new SandTile((byte) tileRegistry.size()));
        tileRegistry.add(new WaterTile((byte) tileRegistry.size()));
        tileRegistry.add(new ShallowWaterTile((byte) tileRegistry.size()));
        tileRegistry.add(new DirtTile((byte) tileRegistry.size()));
        tileRegistry.add(new StoneTile((byte) tileRegistry.size()));
        tileRegistry.add(new BedrockTile((byte) tileRegistry.size()));
        tileRegistry.add(new BrickWallTile((byte) tileRegistry.size()));
        tileRegistry.add(new LadderTile((byte) tileRegistry.size()));

        itemRegistry = new ArrayList<>();
        for (Tile tile : tileRegistry) {
            itemRegistry.add(new TileItem(tile, (byte) tileRegistry.size()));
        }
        itemRegistry.add(new StickItem((byte) itemRegistry.size()));
        itemRegistry.add(new MeatItem((byte) itemRegistry.size()));

        create(size);
    }
}
