package tadeln.csg.world.gen;

import tadeln.csg.Vector2f;

/*
    Copied code from Wikipedia:
    https://en.wikipedia.org/wiki/Perlin_noise#Implementation
    changed some stuff so it works in Java;
 */
public class PerlinNoise {

    public static float interpolate(float a0, float a1, float w) {
        return (1.0f - w)*a0 + w*a1;
    }

    public static Vector2f randomGradient(int ix, int iy) {
        float random = (float) (2920.f * Math.sin(ix * 21942.f + iy * 171324.f + 8912.f) * Math.cos(ix * 23157.f * iy * 217832.f + 9758.f));
        return new Vector2f((float)Math.cos(random), (float)Math.sin(random));
    }

    public static float dotGridGradient(int ix, int iy, float x, float y) {
        // Get gradient from integer coordinates
        Vector2f gradient = randomGradient(ix, iy);

        // Compute the distance vector
        float dx = x - (float)ix;
        float dy = y - (float)iy;

        // Compute the dot-product
        return (dx*gradient.getX() + dy*gradient.getY());
    }

    public static float perlin(float x, float y) {
        // Determine grid cell coordinates
        int x0 = (int)x;
        int x1 = x0 + 1;
        int y0 = (int)y;
        int y1 = y0 + 1;

        // Determine interpolation weights
        // Could also use higher order polynomial/s-curve here
        float sx = x - (float)x0;
        float sy = y - (float)y0;

        // Interpolate between grid point gradients
        float n0, n1, ix0, ix1, value;

        n0 = dotGridGradient(x0, y0, x, y);
        n1 = dotGridGradient(x1, y0, x, y);
        ix0 = interpolate(n0, n1, sx);

        n0 = dotGridGradient(x0, y1, x, y);
        n1 = dotGridGradient(x1, y1, x, y);
        ix1 = interpolate(n0, n1, sx);

        value = interpolate(ix0, ix1, sy);
        return value;
    }
}
