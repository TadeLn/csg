package tadeln.csg.world.gen;

import tadeln.csg.Renderer;
import tadeln.csg.Settings;
import tadeln.csg.world.World;
import tadeln.csg.world.tile.*;

public class WorldGen {

    public static void generate(World world) {
        byte[][][] tiles = new byte[world.getSize().getZ()][world.getSize().getX()][world.getSize().getY()];

        long prevTime = System.currentTimeMillis();
        final int progressIntervalMs = 200;

        final int surfaceLevel = 5;
        final int belowSurfaceLevel = surfaceLevel - 1;
        final int stoneLevel = belowSurfaceLevel - 1;

        final byte grassId = GrassTile.getIdStatic();
        final byte sandId = SandTile.getIdStatic();
        final byte waterId = WaterTile.getIdStatic();
        final byte shallowWaterId = ShallowWaterTile.getIdStatic();
        final byte dirtId = DirtTile.getIdStatic();
        final byte stoneId = StoneTile.getIdStatic();
        final byte bedrockId = BedrockTile.getIdStatic();

        // Generate surface
        for (int x = 0; x < world.getSize().getX(); x++) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - prevTime >= progressIntervalMs) {
                prevTime = currentTime;
                Renderer.print("Generating surface... " + Math.round((float) x / (float) world.getSize().getX() * 100.f) + "%\n");
                Renderer.render();
            }
            for (int y = 0; y < world.getSize().getY(); y++) {
                float result = PerlinNoise.perlin((float)x * Settings.perlinNoiseScaleX, (float)y * Settings.perlinNoiseScaleY);
                result += PerlinNoise.perlin((float)x * Settings.perlinNoiseScale2X, (float)y * Settings.perlinNoiseScale2Y);
                if (result <= -0.1) {
                    tiles[surfaceLevel][x][y] = waterId;
                    tiles[belowSurfaceLevel][x][y] = sandId;
                } else if (result <= 0) {
                    tiles[surfaceLevel][x][y] = shallowWaterId;
                    tiles[belowSurfaceLevel][x][y] = sandId;
                } else if (result <= 0.2) {
                    tiles[surfaceLevel][x][y] = sandId;
                    tiles[belowSurfaceLevel][x][y] = sandId;
                } else {
                    tiles[surfaceLevel][x][y] = grassId;
                    tiles[belowSurfaceLevel][x][y] = dirtId;
                }
            }
        }

        // Fill with stone
        for (int z = stoneLevel; z > 0; z--) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - prevTime >= progressIntervalMs) {
                prevTime = currentTime;
                Renderer.print("Filling with stone... " + Math.round((float) (stoneLevel - z) / (float) stoneLevel * 100.f) + "%\n");
                Renderer.render();
            }
            for (int x = 0; x < world.getSize().getX(); x++) {
                for (int y = 0; y < world.getSize().getY(); y++) {
                    tiles[z][x][y] = stoneId;
                }
            }
        }

        // Generate bedrock
        for (int x = 0; x < world.getSize().getX(); x++) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - prevTime >= progressIntervalMs) {
                prevTime = currentTime;
                Renderer.print("Generating bedrock... " + Math.round((float) x / (float) world.getSize().getX() * 100.f) + "%\n");
                Renderer.render();
            }
            for (int y = 0; y < world.getSize().getY(); y++) {
                tiles[0][x][y] = bedrockId;
            }
        }

        world.setTiles(tiles);
    }
}
