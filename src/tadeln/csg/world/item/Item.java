package tadeln.csg.world.item;

import tadeln.csg.Settings;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.formatting.Formatting;

public abstract class Item {

    protected byte id = -1;

    protected char character;

    protected Formatting formatting;

    protected FormattedString name;

    protected String description;

    protected int stackSize;

    public byte getId() {
        return id;
    }

    public static byte getIdStatic() {
        return -1;
    }

    public char getCharacter() {
        return character;
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public FormattedString getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getStackSize() {
        return stackSize;
    }

    public FormattedStringChain getFullName() {
        return new FormattedStringChain(new FormattedString("" + getCharacter(), formatting))
                .append(" ")
                .append(getName().bold())
                .append(Settings.showIds ? new FormattedString(" #" + getId(), Formatting.WHITE) : new FormattedString(""));
    }

    public Item(byte id) {
        this(id, ' ', new Formatting(), new FormattedString(""));
    }

    public Item(byte id, char character, Formatting formatting, FormattedString name) {
        this.id = id;
        this.character = character;
        this.formatting = formatting;
        this.name = name;
        this.description = "No description provided";
        this.stackSize = 99;
    }
}
