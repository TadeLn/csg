package tadeln.csg.world.item;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class StickItem extends Item {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public StickItem() {
        super(
                id,
                '/',
                new Formatting().color(94, Color.YELLOW),
                new FormattedString("Stick", new Formatting().color(94, Color.YELLOW))
        );
    }

    public StickItem(byte id) {
        this();
        StickItem.id = id;
        super.id = id;
    }
}
