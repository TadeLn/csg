package tadeln.csg.world.item;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class MeatItem extends Item {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public MeatItem() {
        super(
                id,
                'o',
                new Formatting().color(124, Color.RED),
                new FormattedString("Meat", new Formatting().color(124, Color.RED))
        );
    }

    public MeatItem(byte id) {
        this();
        MeatItem.id = id;
        super.id = id;
    }
}
