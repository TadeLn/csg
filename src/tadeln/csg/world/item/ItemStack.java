package tadeln.csg.world.item;

import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.world.World;

public class ItemStack {

    private byte id;

    private int count;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Item getItem() {
        return World.getItemRegistry().get(id);
    }

    public FormattedStringChain getFullName() {
        return getFullName(false);
    }

    public FormattedStringChain getFullName(boolean hideMaxCount) {
        return new FormattedStringChain(getItem().getFullName())
                .append(" x" + getCount())
                .append(hideMaxCount ? "" : ("/" + getItem().getStackSize()));
    }

    public ItemStack(byte id, int count) {
        this.id = id;
        this.count = count;
    }

    public ItemStack(byte id) {
        this(id, 1);
    }

    public ItemStack() {
        this((byte)0, 1);
    }
}
