package tadeln.csg.world.item;

import tadeln.csg.Renderer;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.World;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private List<ItemStack> slots;

    public List<ItemStack> getSlots() {
        return slots;
    }

    public boolean contains(ItemStack itemStack) {
        // TODO Inventory::contains()
        return false;
    }

    public boolean contains(byte id) {
        return contains(new ItemStack(id, 1));
    }

    public Inventory forceStack() {
        for (int i = slots.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                ItemStack stack1 = slots.get(j);
                ItemStack stack2 = slots.get(i);
                if (stack1.getId() == stack2.getId()) {
                    int count = Math.min(
                            World.getItemRegistry().get(stack1.getId()).getStackSize() - stack1.getCount(),
                            stack2.getCount()
                    );
                    stack1.setCount(stack1.getCount() + count);
                    stack2.setCount(stack2.getCount() - count);
                }
            }
        }
        cleanup();
        return this;
    }

    public Inventory cleanup() {
        for (int i = 0; i < slots.size(); i++) {
            if (slots.get(i).getCount() <= 0) {
                slots.remove(i);
                i--;
            }
        }
        return this;
    }

    public Inventory add(ItemStack itemStack) {
        slots.add(itemStack);
        forceStack();
        return this;
    }

    public Inventory add(Inventory inventory) {
        for (ItemStack s : inventory.getSlots()) {
            add(s);
        }
        return this;
    }

    public Inventory remove(ItemStack itemStack) {
        int amountToRemove = itemStack.getCount();
        for (int i = 0; i < slots.size(); i++) {
            ItemStack stack = slots.get(i);
            if (stack.getId() == itemStack.getId()) {
                int amount = Math.min(
                        stack.getCount(),
                        itemStack.getCount()
                );
                stack.setCount(stack.getCount() - amount);
                amountToRemove -= amount;
            }
        }
        if (amountToRemove > 0) {
            Renderer.print(new FormattedString("ERROR: Couldn't remove that many items", Formatting.RED));
        }
        forceStack();
        return this;
    }

    public Inventory clear() {
        slots.clear();
        return this;
    }

    public Inventory() {
        slots = new ArrayList<ItemStack>();
    }
}
