package tadeln.csg.world.item;

import tadeln.csg.world.tile.Tile;

public class TileItem extends Item {

    private Tile tile;

    public byte getId() {
        return tile.getId();
    }

    public TileItem(Tile tile) {
        super(
                tile.getId(),
                tile.getCharacter(),
                tile.getFormatting(),
                tile.getName()
        );
        this.tile = tile;
        this.description = "A tile";
    }

    public TileItem(Tile tile, byte id) {
        this(tile);
        super.id = id;
    }
}
