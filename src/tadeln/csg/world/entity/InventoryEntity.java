package tadeln.csg.world.entity;

import tadeln.csg.world.item.Inventory;

public abstract class InventoryEntity extends Entity {

    protected Inventory inventory;

    public Inventory getInventory() {
        return inventory;
    }

    public InventoryEntity() {
        super();
        inventory = new Inventory();
    }
}
