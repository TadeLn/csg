package tadeln.csg.world.entity;

import tadeln.csg.Settings;
import tadeln.csg.Vector3;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.World;
import tadeln.csg.world.tile.ShallowWaterTile;
import tadeln.csg.world.tile.Tile;

public abstract class Entity {

    private static int nextUuid = 0;

    protected static int assignNewUuid() {
        return nextUuid++;
    }


    protected int uuid;

    protected Vector3 position;

    protected char character;

    protected boolean repeatCharacter;

    protected Formatting formatting;

    protected FormattedString name;

    protected String description;

    protected int age;

    protected int health;

    protected int maxHealth;

    protected boolean isHittable;

    protected boolean dead;

    protected boolean despawned;

    public int getUuid() {
        return uuid;
    }

    public Vector3 getPosition() {
        return position;
    }

    public void setPosition(Vector3 position) {
        this.position = new Vector3(position);
    }

    public char getCharacter() {
        return character;
    }

    public boolean isRepeatCharacter() {
        return repeatCharacter;
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public FormattedString getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getAge() {
        return age;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public boolean isHittable() {
        return isHittable;
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isDespawned() {
        return despawned;
    }

    public void setDespawned() {
        despawned = true;
    }

    public void die(World world) {
        dead = true;
        setDespawned();
    }

    public void move(Vector3 vector, World world) {
        if (world.getTile(position.add(vector)).isSolid()) return;
        this.position = this.position.add(vector);
    }

    public void hit(int damage) {
        if (isHittable) {
            this.health -= damage;
        }
    }

    public void forceFall(World world) {
        Tile tile = world.getTile(position.add(new Vector3(0, 0, -1)));
        while (!(tile.isSolid() || tile.isSemisolid())) {
            this.move(new Vector3(0, 0, -1), world);
            if (position.getZ() < -10) {
                die(world);
                break;
            }
            tile = world.getTile(position.add(new Vector3(0, 0, -1)));
        }
    }

    public boolean onSpawn(World world) {
        age = 0;
        health = maxHealth;
        dead = false;
        return true;
    }

    public void tick(World world) {
        age++;
        forceFall(world);
        if (world.getTileId(position) == ShallowWaterTile.getIdStatic()) {
            this.move(new Vector3(0, 0, 1), world);
        }

        if (health <= 0) {
            die(world);
        }
    }

    public FormattedStringChain getFullName() {
        return new FormattedStringChain(new FormattedString("" + getCharacter(), formatting))
                .append(" ")
                .append(getName().bold())
                .append(new FormattedString("  HP: " + getHealth() + "/" + getMaxHealth(), Formatting.RED))
                .append(Settings.showIds ? new FormattedString("  UUID#" + getUuid(), Formatting.WHITE) : new FormattedString(""));
    }

    public Entity() {
        uuid = assignNewUuid();
        position = new Vector3();
        character = '@';
        repeatCharacter = false;
        formatting = new Formatting();
        name = new FormattedString("Unknown Entity");
        description = "No description provided";
        isHittable = true;
        despawned = false;
    }

}
