package tadeln.csg.world.entity;

import tadeln.csg.Vector2;
import tadeln.csg.Vector3;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.World;
import tadeln.csg.world.item.ItemStack;
import tadeln.csg.world.item.MeatItem;
import tadeln.csg.world.tile.GrassTile;

public class Pig extends Entity {

    @Override
    public void die(World world) {
        super.die(world);
        Entity e = new ItemEntity(new ItemStack(MeatItem.getIdStatic()));
        e.setPosition(getPosition());
        world.spawnEntity(e, true);
    }

    @Override
    public boolean onSpawn(World world) {
        super.onSpawn(world);
        if (world.getTileId(position.add(new Vector3(0, 0, -1))) != GrassTile.getIdStatic()) {
            return false; // If the tile below is not a GrassTile, don't spawn
        }
        Vector2 distanceToPlayer = new Vector2(world.getPlayer().getPosition().subtract(position));
        // If the distance to the player is < 25, don't spawn
        return !(Math.sqrt(Math.pow(distanceToPlayer.getX(), 2) + Math.pow(distanceToPlayer.getY(), 2)) < 25);
    }

    @Override
    public void tick(World world) {
        super.tick(world);
        byte direction = (byte)(Math.random() * 6);
        Vector3 offset = new Vector3();

        switch (direction) {
            case 0: offset.setX(-1); break;
            case 1: offset.setX(1); break;
            case 2: offset.setY(-1); break;
            case 3: offset.setY(1); break;
            default: break;
        }

        // The place you want to move to is a liquid, don't move
        if (!(world.getTile(this.getPosition().add(offset)).isLiquid() && Math.random() < 0.8)) {
            this.move(offset, world);
        }
    }

    public Pig() {
        super();
        character = '8'; // @ ඞ ⬤
        formatting = new Formatting().color(207, Color.BRIGHT_PURPLE);
        repeatCharacter = false;
        name = new FormattedString("Pig", new Formatting().color(207, Color.BRIGHT_PURPLE));
        description = "A nice round pig; looks like a tasty dinner";
        maxHealth = 5;
    }
}
