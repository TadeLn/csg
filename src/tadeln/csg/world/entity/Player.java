package tadeln.csg.world.entity;

import tadeln.csg.Vector2;
import tadeln.csg.Vector3;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.World;
import tadeln.csg.world.tile.WaterTile;

public class Player extends InventoryEntity {

    protected String nickname;

    public String getNickname() {
        return nickname;
    }

    public void randomTeleport(World world) {
        Vector3 size = world.getSize();
        setPosition(new Vector3((int) (Math.random() * size.getX()), (int) (Math.random() * size.getY()), size.getZ()));
        forceFall(world);
    }

    @Override
    public boolean onSpawn(World world) {
        super.onSpawn(world);
        Vector2 distanceToCenter;
        do {
            setPosition(new Vector3((int) (Math.random() * world.getSize().getX()), (int) (Math.random() * world.getSize().getY()), world.getSize().getZ()));
            distanceToCenter = new Vector2(world.getSize().divide(2).subtract(position));
        } while (
                world.getTileId(new Vector3(position).setZ(world.getHeight(new Vector2(position)))) == WaterTile.getIdStatic() // If in water
                || Math.abs(distanceToCenter.getX()) + Math.abs(distanceToCenter.getY()) > 400 // or if the Manhattan distance to the center of the world > 400
        ); // pick a new location
        forceFall(world);
        return true;
    }

    @Override
    public void die(World world) {
        inventory.clear();
        randomTeleport(world);
    }

    public Player(String nickname) {
        super();
        character = 'ඞ'; // @ ඞ ⬤
        formatting = new Formatting().color(21, Color.BLUE);
        repeatCharacter = false;
        name = new FormattedString("Player", new Formatting(Color.BLUE));
        description = "It's you!";
        maxHealth = 50;
        this.nickname = nickname;
    }
}
