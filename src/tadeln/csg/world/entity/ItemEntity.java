package tadeln.csg.world.entity;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.item.Inventory;
import tadeln.csg.world.item.ItemStack;

public class ItemEntity extends InventoryEntity {

    @Override
    public String getDescription() {
        FormattedStringChain result = new FormattedStringChain();
        for (int i = 0; i < inventory.getSlots().size(); i++) {
            ItemStack s = inventory.getSlots().get(i);
            result.append(s.getFullName());

            if (i != inventory.getSlots().size() - 1) {
                result.append(", ");
            }
        }
        return result.toString();
    }

    public ItemEntity(ItemStack itemStack) {
        this(new Inventory().add(itemStack));
    }

    public ItemEntity(Inventory inventory) {
        super();
        character = '$';
        formatting = new Formatting().color(255, Color.BRIGHT_WHITE);
        repeatCharacter = false;
        name = new FormattedString("Item", new Formatting().color(255, Color.BRIGHT_WHITE));
        maxHealth = 1;
        isHittable = false;
        this.inventory = inventory;
    }
}
