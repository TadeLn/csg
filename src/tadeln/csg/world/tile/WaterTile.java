package tadeln.csg.world.tile;

import tadeln.csg.Vector2;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class WaterTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    @Override
    public char getCharacter(Vector2 location) {
        if (Math.random() * 2 >= 1) {
            return '~';
        } else {
            return '.';
        }
    }

    public WaterTile() {
        super(
                id,
                '~',
                new Formatting(Color.WHITE).background(27, Color.BLUE),
                new FormattedString("Water", new Formatting().color(27, Color.BLUE))
        );
        description = "A liquid that you can swim in";
        isSolid = false;
        isLiquid = true;
    }

    public WaterTile(byte id) {
        this();
        WaterTile.id = id;
        super.id = id;
    }
}
