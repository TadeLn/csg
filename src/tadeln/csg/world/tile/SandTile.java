package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class SandTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public SandTile() {
        super(
                id,
                '.',
                new Formatting(Color.WHITE).background(143, Color.BRIGHT_YELLOW),
                new FormattedString("Sand", new Formatting().color(143, Color.BRIGHT_YELLOW))
        );
        description = "Yellow sand found near water";
    }

    public SandTile(byte id) {
        this();
        SandTile.id = id;
        super.id = id;
    }
}
