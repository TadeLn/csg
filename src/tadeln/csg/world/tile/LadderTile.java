package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class LadderTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public LadderTile() {
        super(
                id,
                '#',
                new Formatting().color(94, Color.YELLOW),
                new FormattedString("Ladder", new Formatting().color(94, Color.YELLOW))
        );
        description = "A ladder that you can climb";
        isSolid = false;
        isSemisolid = true;
    }

    public LadderTile(byte id) {
        this();
        LadderTile.id = id;
        super.id = id;
    }
}
