package tadeln.csg.world.tile;

import tadeln.csg.Vector2;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

import java.util.Random;

public class GrassTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    private static double getMagicNumber(Vector2 position) {
        Random generator = new Random(position.getX() + (position.getY() * 10000));
        generator.nextDouble();
        return generator.nextDouble();
    }

    @Override
    public char getCharacter(Vector2 position) {
        double randomNumber = getMagicNumber(position) * 100;
        if (randomNumber <= 49) {
            return '\"';
        } else if (randomNumber <= 97) {
            return '\'';
        } else {
            return '*';
        }
    }

    @Override
    public Formatting getFormatting(Vector2 position) {
        double randomNumber = getMagicNumber(position) * 100;
        if (randomNumber <= 97) {
            return new Formatting().color(28, Color.WHITE).background(34, Color.BRIGHT_GREEN);
        } else if (randomNumber <= 98) {
            return new Formatting().color(160, Color.RED).background(34, Color.BRIGHT_GREEN);
        } else {
            return new Formatting().color(184, Color.YELLOW).background(34, Color.BRIGHT_GREEN);
        }
    }

    public GrassTile() {
        super(
                id,
                '\"',
                new Formatting().color(28, Color.GREEN).background(34,Color.BRIGHT_GREEN),
                new FormattedString("Grass", new Formatting().color(34, Color.BRIGHT_GREEN))
        );
        description = "Dirt with some grass and flowers on top of it";
    }

    public GrassTile(byte id) {
        this();
        GrassTile.id = id;
        super.id = id;
    }
}
