package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class StoneTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public StoneTile() {
        super(
                id,
                'X',
                new Formatting(Color.BRIGHT_WHITE).background(248, Color.WHITE),
                new FormattedString("Stone", new Formatting().color(248, Color.WHITE))
        );
        description = "A solid stone found underground";
    }

    public StoneTile(byte id) {
        this();
        StoneTile.id = id;
        super.id = id;
    }
}
