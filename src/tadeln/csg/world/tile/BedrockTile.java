package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class BedrockTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public BedrockTile() {
        super(
                id,
                '░',
                new Formatting(Color.BRIGHT_BLACK).background(234, Color.BLACK),
                new FormattedString("Bedrock", new Formatting().color(234, Color.WHITE))
        );
        description = "An unbreakable rock found at the bottom of the world";
        isUnbreakable = true;
    }

    public BedrockTile(byte id) {
        this();
        BedrockTile.id = id;
        super.id = id;
    }
}
