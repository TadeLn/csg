package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class BrickWallTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public BrickWallTile() {
        super(
                id,
                '▒',
                new Formatting(Color.WHITE).background(52, Color.RED),
                new FormattedString("Brick Wall", new Formatting().color(52, Color.RED))
        );
        description = "Solid brick wall";
    }

    public BrickWallTile(byte id) {
        this();
        BrickWallTile.id = id;
        super.id = id;
    }
}
