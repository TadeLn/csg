package tadeln.csg.world.tile;

import tadeln.csg.Vector2;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class ShallowWaterTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    @Override
    public char getCharacter(Vector2 location) {
        if (Math.random() * 5 >= 4) {
            return '~';
        } else {
            return '.';
        }
    }

    public ShallowWaterTile() {
        super(
                id,
                '~',
                new Formatting(Color.WHITE).background(32, Color.BRIGHT_BLUE),
                new FormattedString("Shallow Water", new Formatting().color(32, Color.BRIGHT_BLUE))
        );
        description = "Shallow water that you can walk on";
        isSolid = false;
        isLiquid = true;
    }

    public ShallowWaterTile(byte id) {
        this();
        ShallowWaterTile.id = id;
        super.id = id;
    }
}
