package tadeln.csg.world.tile;

import tadeln.csg.Settings;
import tadeln.csg.Vector2;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.formatting.Formatting;

public abstract class Tile {

    protected Byte id = -1;

    protected char character;

    protected Formatting formatting;

    protected FormattedString name;

    protected String description;

    protected boolean isSolid = true;

    protected boolean isSemisolid = false;

    protected boolean isLiquid = false;

    protected boolean isUnbreakable = false;

    public byte getId() { return id; }

    public static byte getIdStatic() { return -1; }

    public char getCharacter() {
        return character;
    }

    public char getCharacter(Vector2 location) {
        return getCharacter();
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public Formatting getFormatting(Vector2 location) { return getFormatting(); }

    public FormattedString getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isSolid() {
        return isSolid;
    }

    public boolean isSemisolid() {
        return isSemisolid;
    }

    public boolean isLiquid() {
        return isLiquid;
    }

    public boolean isUnbreakable() {
        return isUnbreakable;
    }

    public FormattedStringChain getFullName() {
        return new FormattedStringChain(new FormattedString("" + getCharacter(), formatting))
                .append(" ")
                .append(getName().bold())
                .append(Settings.showIds ? new FormattedString(" #" + getId(), Formatting.WHITE) : new FormattedString(""));
    }

    public Tile(byte id) {
        this(id, ' ', new Formatting(), new FormattedString(""));
    }

    public Tile(byte id, char character, Formatting formatting, FormattedString name) {
        this.id = id;
        this.character = character;
        this.formatting = formatting;
        this.name = name;
        this.description = "No description provided";
    }
}
