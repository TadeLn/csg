package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class AirTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public AirTile() {
        super(
                id,
                ' ',
                new Formatting(Color.WHITE ),
                new FormattedString("Air", Formatting.WHITE)
        );
        description = "78% nitrogen and 21% oxygen.";
        isSolid = false;
    }

    public AirTile(byte id) {
        this();
        AirTile.id = id;
        super.id = id;
    }
}
