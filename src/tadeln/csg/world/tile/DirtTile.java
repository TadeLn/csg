package tadeln.csg.world.tile;

import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class DirtTile extends Tile {

    private static byte id = -1;

    public static byte getIdStatic() {
        return id;
    }

    public DirtTile() {
        super(
                id,
                '%',
                new Formatting(Color.BLACK).background(94, Color.YELLOW),
                new FormattedString("Dirt", new Formatting().color(94, Color.YELLOW))
        );
        description = "Dirt from just below the ground";
    }

    public DirtTile(byte id) {
        this();
        DirtTile.id = id;
        super.id = id;
    }
}
