package tadeln.csg.formatting;

import tadeln.csg.Settings;

public class FormattedString {

    private Formatting formatting;

    private String string;

    public Formatting getFormatting() {
        return formatting;
    }

    public FormattedString setFormatting(Formatting formatting) {
        this.formatting = formatting;
        return this;
    }

    public String getString() {
        return string;
    }

    public FormattedString setString(String string) {
        this.string = string;
        return this;
    }

    // Methods from Formatting
    public FormattedString color(int color) {
        formatting.color(color);
        return this;
    }

    public FormattedString color(int extColor, int color) {
        formatting.color(extColor, color);
        return this;
    }

    public FormattedString background(int background) {
        formatting.background(background);
        return this;
    }

    public FormattedString background(int extBackground, int background) {
        formatting.background(extBackground, background);
        return this;
    }

    public FormattedString bold() {
        formatting.bold();
        return this;
    }

    public FormattedString underline() {
        formatting.underline();
        return this;
    }

    public FormattedString reverse() {
        formatting.reverse();
        return this;
    }

    public int getColor() {
        return formatting.getColor();
    }

    public int getBackground() {
        return formatting.getBackground();
    }

    public boolean isBold() {
        return formatting.isBold();
    }

    public boolean isUnderline() {
        return formatting.isUnderline();
    }

    public boolean isReversed() {
        return formatting.isReversed();
    }

    public boolean isReset() {
        return formatting.isReset();
    }

    public String toString() {
        if (Settings.useColor) {
            return formatting.getAnsiString() + string;
        } else {
            return string;
        }
    }

    public FormattedString(String string, Formatting formatting) {
        this.formatting = formatting;
        this.string = string;
    }

    public FormattedString(String string) {
        this(string, new Formatting());
    }
}
