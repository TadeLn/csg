package tadeln.csg.formatting;

import java.util.ArrayList;
import java.util.List;

public class FormattedStringChain extends FormattedString {

    private List<FormattedString> formattedStrings;

    // Methods from FormattedString
    public Formatting getFormatting() {
        return null;
    }

    public FormattedStringChain setFormatting(Formatting formatting) {
        for (FormattedString x : formattedStrings) {
            x.setFormatting(formatting);
        }
        return this;
    }

    public String getString() {
        String result = "";
        for (FormattedString x : formattedStrings) {
            result += x.getString();
        }
        return result;
    }

    public FormattedStringChain setString(String string) {
        clear();
        append(new FormattedString(string));
        return this;
    }

    // Methods from Formatting
    public FormattedStringChain color(int color) {
        for (FormattedString x : formattedStrings) {
            x.color(color);
        }
        return this;
    }

    public FormattedStringChain color(int extColor, int color) {
        for (FormattedString x : formattedStrings) {
            x.color(extColor, color);
        }
        return this;
    }

    public FormattedStringChain background(int background) {
        for (FormattedString x : formattedStrings) {
            x.background(background);
        }
        return this;
    }

    public FormattedStringChain background(int extBackground, int background) {
        for (FormattedString x : formattedStrings) {
            x.background(extBackground, background);
        }
        return this;
    }

    public FormattedStringChain bold() {
        for (FormattedString x : formattedStrings) {
            x.bold();
        }
        return this;
    }

    public FormattedStringChain underline() {
        for (FormattedString x : formattedStrings) {
            x.underline();
        }
        return this;
    }

    public FormattedStringChain reverse() {
        for (FormattedString x : formattedStrings) {
            x.reverse();
        }
        return this;
    }

    public int getColor() {
        return 0;
    }

    public int getBackground() {
        return 0;
    }

    public boolean isBold() {
        return false;
    }

    public boolean isUnderline() {
        return false;
    }

    public boolean isReversed() {
        return false;
    }

    public boolean isReset() {
        return false;
    }


    public FormattedStringChain clear() {
        formattedStrings.clear();
        return this;
    }

    public List<FormattedString> getParts() {
        return formattedStrings;
    }

    public FormattedStringChain append(FormattedStringChain formattedStringChain) {
        for (FormattedString x : formattedStringChain.getParts()) {
            formattedStrings.add(x);
        }
        return this;
    }

    public FormattedStringChain append(FormattedString formattedString) {
        formattedStrings.add(formattedString);
        return this;
    }

    public FormattedStringChain append(String string) {
        append(new FormattedString(string));
        return this;
    }

    public String toString() {
        String result = "";
        for (FormattedString x : formattedStrings) {
            result += x.toString();
        }
        return result;
    }

    public FormattedStringChain() {
        super("");
        formattedStrings = new ArrayList<FormattedString>();
    }

    public FormattedStringChain(FormattedStringChain formattedStringChain) {
        this();
        append(formattedStringChain);
    }

    public FormattedStringChain(FormattedString formattedString) {
        this();
        formattedStrings.add(formattedString);
    }

    public FormattedStringChain(String string) {
        this(new FormattedString(string));
    }
}
