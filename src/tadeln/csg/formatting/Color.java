package tadeln.csg.formatting;

public class Color {
    public static final int BLACK = 0x0;
    public static final int RED = 0x1;
    public static final int GREEN = 0x2;
    public static final int YELLOW = 0x3;
    public static final int BLUE = 0x4;
    public static final int PURPLE = 0x5;
    public static final int CYAN = 0x6;
    public static final int WHITE = 0x7;

    public static final int BRIGHT_BLACK = 0x8;
    public static final int BRIGHT_RED = 0x9;
    public static final int BRIGHT_GREEN = 0xa;
    public static final int BRIGHT_YELLOW = 0xb;
    public static final int BRIGHT_BLUE = 0xc;
    public static final int BRIGHT_PURPLE = 0xd;
    public static final int BRIGHT_CYAN = 0xe;
    public static final int BRIGHT_WHITE = 0xf;
}
