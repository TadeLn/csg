package tadeln.csg.formatting;

import tadeln.csg.Settings;

public class Formatting {

    public static final Formatting NONE = new Formatting();

    public static final Formatting BLACK = new Formatting(Color.BLACK);
    public static final Formatting RED = new Formatting(Color.RED);
    public static final Formatting GREEN = new Formatting(Color.GREEN);
    public static final Formatting YELLOW = new Formatting(Color.YELLOW);
    public static final Formatting BLUE = new Formatting(Color.BLUE);
    public static final Formatting PURPLE = new Formatting(Color.PURPLE);
    public static final Formatting CYAN = new Formatting(Color.CYAN);
    public static final Formatting WHITE = new Formatting(Color.WHITE);

    private int color;

    private int background;

    private boolean bold;

    private boolean underline;

    private boolean reversed;

    private boolean reset;

    public String getAnsiString() {
        String result = "\u001b[0m";

        if (reset) {
            return result;
        }

        int color = this.color;
        int background = this.background;

        if (Settings.reverseBlackWhite) {
            if (color == Color.BLACK) color = Color.BRIGHT_WHITE;
            else if (color == Color.BRIGHT_BLACK) color = Color.BRIGHT_BLACK;
            else if (color == Color.WHITE) color = Color.WHITE;
            else if (color == Color.BRIGHT_WHITE) color = Color.BLACK;
            if (background == Color.BLACK) background = Color.BRIGHT_WHITE;
            else if (background == Color.BRIGHT_BLACK) background = Color.BRIGHT_BLACK;
            else if (background == Color.WHITE) background = Color.WHITE;
            else if (background == Color.BRIGHT_WHITE) background = Color.BLACK;
        }
        if (!Settings.extraColors) {
            if (color != -1) {
                if (color <= 0x7) {
                    // Normal colors
                    result += "\u001b[" + (30 + color) + "m";
                } else {
                    // Bright colors
                    result += "\u001b[" + (30 + color - 0x8) + ";1m";
                }
            }
            if (background != -1) {
                if (background <= 0x7) {
                    // Normal colors
                    result += "\u001b[" + (40 + background) + "m";
                } else {
                    // Bright colors
                    result += "\u001b[" + (40 + background - 0x8) + ";1m";
                }
            }
        } else {
            if (color != -1) {
                result += "\u001b[38;5;" + color + "m";
            }
            if (background != -1) {
                result += "\u001b[48;5;" + background + "m";
            }
        }
        if (bold) {
            result += "\u001b[1m";
        }
        if (underline) {
            result += "\u001b[4m";
        }
        if (reversed) {
            result += "\u001b[7m";
        }

        return result;
    }

    public Formatting color(int color) {
        this.reset = false;
        this.color = color;
        return this;
    }

    public Formatting color(int extColor, int color) {
        if (Settings.extraColors) {
            return this.color(extColor);
        } else {
            return this.color(color);
        }
    }

    public Formatting background(int background) {
        this.reset = false;
        this.background = background;
        return this;
    }

    public Formatting background(int extBackground, int background) {
        if (Settings.extraColors) {
            return this.background(extBackground);
        } else {
            return this.background(background);
        }
    }

    public Formatting bold() {
        this.reset = false;
        bold = true;
        return this;
    }

    public Formatting underline() {
        this.reset = false;
        underline = true;
        return this;
    }

    public Formatting reverse() {
        this.reset = false;
        reversed = true;
        return this;
    }

    public int getColor() {
        return color;
    }

    public int getBackground() {
        return background;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isUnderline() {
        return underline;
    }

    public boolean isReversed() {
        return reversed;
    }

    public boolean isReset() {
        return reset;
    }

    public Formatting() {
        this.reset = true;

        this.color = -1;
        this.background = -1;

        this.bold = false;
        this.underline = false;
        this.reversed = false;
    }

    public Formatting(int color) {
        this.reset = false;

        this.color = color;
        this.background = -1;

        this.bold = false;
        this.underline = false;
        this.reversed = false;
    }

    public Formatting(Formatting formatting) {
        this.reset = formatting.isReset();

        this.color = formatting.getColor();
        this.background = formatting.getBackground();

        this.bold = formatting.isBold();
        this.underline = formatting.isUnderline();
        this.reversed = formatting.isReversed();
    }

}
