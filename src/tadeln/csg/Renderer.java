package tadeln.csg;

import tadeln.csg.formatting.FormattedString;

public class Renderer {

    private static StringBuilder printBuffer = new StringBuilder();
    private static StringBuilder printOnLoopBuffer = new StringBuilder();

    public static void print(FormattedString formattedString) {
        printBuffer.append(formattedString.toString());
    }

    public static void print(String string) {
        print(new FormattedString(string));
    }

    public static void printOnLoop(FormattedString formattedString) {
        printOnLoopBuffer.append(formattedString.toString());
    }

    public static void printOnLoop(String string) {
        printOnLoop(new FormattedString(string));
    }

    public static void loop() {
        int count = (int) printOnLoopBuffer.chars().filter(ch -> ch == '\n').count();
        print(printOnLoopBuffer.toString());
        print("\n".repeat(Math.max(Settings.printlines - 1 - count, 0)));
        printOnLoopBuffer.setLength(0);
    }

    public static void render() {
        System.out.print(printBuffer);
        printBuffer.setLength(0);
    }
}
