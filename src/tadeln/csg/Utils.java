package tadeln.csg;

public class Utils {

    public static String center(String string, int length, char spaceChar) {
        String result = "" + string;
        while (result.length() < length) {
            result = result + spaceChar;
            if (result.length() < length) {
                result = spaceChar + result;
            }
        }
        return result;
    }

    public static String center(String string, int length) {
        return center(string, length, ' ');
    }

    public static String paddingLeft(String string, int length, char spaceChar) {
        String result = "" + string;
        while (result.length() < length) {
            result = spaceChar + result;
        }
        return result;
    }

    public static String paddingLeft(String string, int length) {
        return paddingLeft(string, length, ' ');
    }

    public static String paddingRight(String string, int length, char spaceChar) {
        String result = "" + string;
        while (result.length() < length) {
            result = result + spaceChar;
        }
        return result;
    }

    public static String paddingRight(String string, int length) {
        return paddingRight(string, length, ' ');
    }

    public static int tryParseInt(String string) {
        int result;
        try {
            result = Integer.parseInt(string);
        } catch (NumberFormatException e) {
            result = 0;
        }
        return result;
    }

    public static boolean isInt(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isInRange(double number, double min, double max) {
        return number >= min && number <= max;
    }

    public static String stripWhitespace(String input) {
        String result = input;
        if (result.isEmpty()) return result;

        while (isWhitespace(result.charAt(0))) {
            result = result.substring(1);
        }

        while (isWhitespace(result.charAt(result.length()-1))) {
            result = result.substring(0, result.length()-1);
        }

        return result;
    }

    public static boolean isWhitespace(char character) {
        return character == ' ' || character == '\t' || character == '\n';
    }

    public static byte[] intToBytes(int number) {
        byte[] arr = new byte[4];
        arr[0] = (byte) (number >> 24 % 0x100);
        arr[1] = (byte) (number >> 16 % 0x100);
        arr[2] = (byte) (number >> 8 % 0x100);
        arr[3] = (byte) (number % 0x100);

        return arr;
    }

    public static int bytesToInt(byte[] arr) {
        return (Byte.toUnsignedInt(arr[0]) << 24) |
                (Byte.toUnsignedInt(arr[1]) << 16) |
                (Byte.toUnsignedInt(arr[2]) << 8) |
                Byte.toUnsignedInt(arr[3]);
    }
}
