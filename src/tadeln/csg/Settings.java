package tadeln.csg;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Settings {

    public static boolean useColor = true;
    public static boolean extraColors = true;
    public static boolean doubleTileWidth = true;
    public static boolean reverseBlackWhite = false;


    public static int windowWidth = 90;
    public static int windowHeight = 45;
    public static int printlines = 5;
    public static boolean showIds = false;


    public static float perlinNoiseScaleX = 0.02f;
    public static float perlinNoiseScaleY = 0.02f;
    public static float perlinNoiseScale2X = 0.07f;
    public static float perlinNoiseScale2Y = 0.07f;


    public static int worldSizeX = 1000;
    public static int worldSizeY = 1000;
    public static int worldSizeZ = 10;


    public static boolean debug = false;


    public static void loadFromFile() {
        final String filename = "settings.txt";
        Renderer.print("Loading settings from " + filename + "...\n");
        try {
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                Settings.parseLine(data);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            Renderer.print("File not found\n");
        }
        Renderer.render();
    }

    public static void loadFromArgs(String[] args) throws Exception {
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("--")) {
                if (args.length > i + 1) {
                    setSetting(args[i].substring(2), args[i + 1]);
                } else {
                    setSetting(args[i].substring(2), "1");
                }
            }
        }
        Renderer.render();
    }

    public static void parseLine(String line) {
        String name = "";
        String value = "";
        int i = 0;

        for (; i < line.length(); i++) {
            if (line.charAt(i) == '=') break;
            name += line.charAt(i);
        }
        i++;
        for (; i < line.length(); i++) {
            value += line.charAt(i);
        }

        name = Utils.stripWhitespace(name);
        value = Utils.stripWhitespace(value);

        setSetting(name, value);
    }

    public static void setSetting(String name, String value) {
        switch (name) {
            case "useColor": useColor = Utils.tryParseInt(value) > 0; break;
            case "extraColors": extraColors = Utils.tryParseInt(value) > 0; break;
            case "doubleTileWidth": doubleTileWidth = Utils.tryParseInt(value) > 0; break;
            case "reverseBlackWhite": reverseBlackWhite = Utils.tryParseInt(value) > 0; break;


            case "windowWidth": windowWidth = Utils.tryParseInt(value); break;
            case "windowHeight": windowHeight = Utils.tryParseInt(value); break;
            case "printLines": printlines = Utils.tryParseInt(value); break;
            case "showIds": showIds = Utils.tryParseInt(value) > 0; break;


            case "perlinNoiseScaleX": perlinNoiseScaleX = Utils.tryParseInt(value); break;
            case "perlinNoiseScaleY": perlinNoiseScaleY = Utils.tryParseInt(value); break;
            case "perlinNoiseScale2X": perlinNoiseScale2X = Utils.tryParseInt(value); break;
            case "perlinNoiseScale2Y": perlinNoiseScale2Y = Utils.tryParseInt(value); break;


            case "worldSizeX": worldSizeX = Utils.tryParseInt(value); break;
            case "worldSizeY": worldSizeY = Utils.tryParseInt(value); break;
            case "worldSizeZ": worldSizeZ = Utils.tryParseInt(value); break;


            case "debug": debug = Utils.tryParseInt(value) > 0; break;

            default:
                Renderer.print("Unknown setting: " + name + "\n");
        }
    }
}
