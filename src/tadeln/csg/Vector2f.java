package tadeln.csg;

public class Vector2f {

    private float x;

    private float y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Vector2f add(Vector2f vector) {
        Vector2f result = new Vector2f(this);
        result.setX(result.getX() + vector.getX());
        result.setY(result.getY() + vector.getY());
        return result;
    }

    public Vector2f subtract(Vector2f vector) {
        Vector2f result = new Vector2f(this);
        result.setX(result.getX() - vector.getX());
        result.setY(result.getY() - vector.getY());
        return result;
    }

    public Vector2f invert() {
        Vector2f result = new Vector2f(this);
        result.setX(-result.getX());
        result.setY(-result.getY());
        return result;
    }

    public Vector2f multiply(float number) {
        Vector2f result = new Vector2f(this);
        result.setX(result.getX() * number);
        result.setY(result.getY() * number);
        return result;
    }

    public Vector2f divide(float number) {
        Vector2f result = new Vector2f(this);
        result.setX(result.getX() / number);
        result.setY(result.getY() / number);
        return result;
    }

    public Vector2f() {
        this(0, 0);
    }

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2f(Vector2f vector) {
        this(vector.getX(), vector.getY());
    }
}
