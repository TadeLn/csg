package tadeln.csg;

public class Debug {

    private static String stringChain;

    public static String get() {
        return stringChain;
    }

    public static void add(String string) {
        stringChain += string;
    }

    public static void clear() {
        stringChain = "";
    }
}
