package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NoEntitySelected;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.world.entity.Entity;

import java.util.ArrayList;
import java.util.List;

public class HitCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        List<Entity> entities = new ArrayList<>();
        for (Entity e : game.getWorld().getEntitiesAtLocation(game.getSelectedTileLocation())) {
            if (e.isHittable()) {
                entities.add(e);
            }
        }
        Entity entity = game.queryEntity(entities);
        if (entity == null) {
            throw new NoEntitySelected();
        }
        entity.hit(1);
        if (entity.getUuid() == game.getPlayer().getUuid()) {
            Renderer.printOnLoop(new FormattedStringChain("You punched yourself... for some reason"));
        } else {
            Renderer.printOnLoop("You hit " + entity.getFullName());
        }
    }

    public HitCommand() {
        super(true, true);
        aliases.add("r");
        aliases.add("hit");
        shortDescription = "Hit an entity";
    }
}
