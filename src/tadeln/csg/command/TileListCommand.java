package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.world.World;
import tadeln.csg.world.tile.Tile;

import java.util.List;

public class TileListCommand extends Command {

    public void run(Game game) {
        List<Tile> registry = World.getTileRegistry();
        for (Tile tile : registry) {
            Renderer.print(tile.getFullName());
            if (tile.isSolid()) {
                Renderer.print(" solid");
            }
            if (tile.isSemisolid()) {
                Renderer.print(" climbable");
            }
            if (tile.isLiquid()) {
                Renderer.print(" liquid");
            }
            if (tile.isUnbreakable()) {
                Renderer.print(" unbreakable");
            }
            Renderer.print("\n");
        }
    }

    public TileListCommand() {
        super(false, false);
        aliases.add("tilelist");
        shortDescription = "Display a list of all tiles";
    }

}
