package tadeln.csg.command;

import tadeln.csg.Game;

public class LookCommand extends Command {

    public void run(Game game) {

    }

    public LookCommand() {
        super(false, true);
        aliases.add("look");
        aliases.add("");
        shortDescription = "Draw the world to the screen";
    }

}
