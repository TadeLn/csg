package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.world.World;
import tadeln.csg.world.item.Item;

public class ItemListCommand extends Command {

    public void run(Game game) {
        for (Item i : World.getItemRegistry()) {
            Renderer.print(new FormattedStringChain().append(i.getFullName()).append("\n"));
        }
    }

    public ItemListCommand() {
        super(false, false);
        aliases.add("itemlist");
        shortDescription = "Display a list of all items";
    }

}
