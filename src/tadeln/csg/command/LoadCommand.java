package tadeln.csg.command;

import tadeln.csg.Game;

public class LoadCommand extends Command {

    public void run(Game game) {
        game.getWorld().load();
    }

    public LoadCommand() {
        super(false, true);
        aliases.add("load");
        shortDescription = "Load the world from a file";
    }

}
