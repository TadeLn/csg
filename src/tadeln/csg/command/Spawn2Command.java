package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NumberOutOfRange;
import tadeln.csg.world.World;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.world.entity.ItemEntity;
import tadeln.csg.world.item.ItemStack;

public class Spawn2Command extends Command {

    public void run(Game game) throws InvalidCommandException {
        int id = 10;

        if (super.getCommandParts().size() >= 2) {
            if (Utils.isInt(super.getPart(1))) {
                id = Utils.tryParseInt(super.getPart(1));
                if (!Utils.isInRange(id, 0, World.getItemRegistry().size()-1)) {
                    throw new NumberOutOfRange(id, 0, World.getItemRegistry().size()-1);
                }
            } else {
                throw new NotAnIntegerException(super.getPart(1));
            }
        }

        Entity e = new ItemEntity(new ItemStack((byte) id));
        e.setPosition(game.getPlayer().getPosition().add(new Vector3(0, 2, 0)));
        game.getWorld().spawnEntity(e, true);
    }

    public Spawn2Command() {
        super(false, true);
        aliases.add("spawn2");
        shortDescription = "Spawn an item";
    }

}
