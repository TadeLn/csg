package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSelectionNorthCommand extends MoveSelectionCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(0, -1, 0));
    }

    public MoveSelectionNorthCommand() {
        aliases.add("i");
        shortDescription = "Move the selection north";
    }
}
