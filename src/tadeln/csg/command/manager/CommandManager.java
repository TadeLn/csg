package tadeln.csg.command.manager;

import tadeln.csg.Settings;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.MissingQuoteEndException;
import tadeln.csg.command.exceptions.UnknownCommandException;
import tadeln.csg.command.*;

import java.util.ArrayList;
import java.util.List;

public class CommandManager {

    private List<Command> commandRegistry;

    public List<Command> getCommandRegistry() {
        return commandRegistry;
    }

    public List<String> parseCommandParts(String input) throws InvalidCommandException {
        List<String> commandParts = new ArrayList<>();
        String currentPart = "";
        boolean insideQuote = false;

        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '"') {
                // Quote
                // End quote
                insideQuote = !insideQuote;
            } else if (input.charAt(i) == ' ' && !insideQuote) {
                // New part
                commandParts.add(currentPart);
                currentPart = "";
            } else if (input.charAt(i) == '\\') {
                // Escaping characters
                if (input.length() <= i + 1) {
                    throw new InvalidCommandException(); // When escaping nothing
                } else {
                    i++;

                    switch (input.charAt(i)) {
                        case '\\':
                            currentPart += '\\';
                            break;

                        case '"':
                            currentPart += '"';
                            break;

                        case ' ':
                            currentPart += ' ';
                            break;

                        case 'n':
                            currentPart += '\n';
                            break;

                        case 't':
                            currentPart += '\t';
                            break;
                    }
                }
            } else {
                currentPart += input.charAt(i);
            }
        }
        if (insideQuote) {
            throw new MissingQuoteEndException();
        }

        commandParts.add(currentPart);
        return commandParts;
    }

    public Command parseCommand(List<String> commandParts) throws UnknownCommandException {
        for (Command x : commandRegistry) {
            List<String> aliases = x.getAliases();
            for (String alias : aliases) {
                if (alias.equals(commandParts.get(0))) {
                    x.setCommandParts(commandParts);
                    return x;
                }
            }
        }
        throw new UnknownCommandException(commandParts.get(0));
    }

    public Command parseCommand(String input) throws InvalidCommandException {
        return parseCommand(parseCommandParts(input));
    }

    public CommandManager() {
        commandRegistry = new ArrayList<>();
        commandRegistry.add(new EchoCommand());
        commandRegistry.add(new ExitCommand());
        commandRegistry.add(new SetSelectionCommand());
        commandRegistry.add(new MoveNorthCommand());
        commandRegistry.add(new MoveSouthCommand());
        commandRegistry.add(new MoveWestCommand());
        commandRegistry.add(new MoveEastCommand());
        commandRegistry.add(new MoveUpCommand());
        commandRegistry.add(new MoveDownCommand());
        commandRegistry.add(new WaitCommand());
        commandRegistry.add(new RestCommand());
        commandRegistry.add(new TileListCommand());
        commandRegistry.add(new BuildCommand());
        commandRegistry.add(new SaveCommand());
        commandRegistry.add(new LoadCommand());
        commandRegistry.add(new LookCommand());
        commandRegistry.add(new HelpCommand());
        commandRegistry.add(new MineCommand());
        commandRegistry.add(new MoveSelectionNorthCommand());
        commandRegistry.add(new MoveSelectionSouthCommand());
        commandRegistry.add(new MoveSelectionEastCommand());
        commandRegistry.add(new MoveSelectionWestCommand());
        commandRegistry.add(new MoveSelectionUpCommand());
        commandRegistry.add(new MoveSelectionDownCommand());
        commandRegistry.add(new InventoryCommand());
        commandRegistry.add(new ItemListCommand());
        commandRegistry.add(new ExamineCommand());
        commandRegistry.add(new PickUpCommand());
        commandRegistry.add(new HitCommand());

        if (Settings.debug) {
            commandRegistry.add(new TestCommand());
            commandRegistry.add(new ColorsCommand());
            commandRegistry.add(new RandomTeleportCommand());
            commandRegistry.add(new TeleportCommand());
            commandRegistry.add(new GiveCommand());
            commandRegistry.add(new SpawnCommand());
            commandRegistry.add(new ToggleDebugCommand());
            commandRegistry.add(new Spawn2Command());
            commandRegistry.add(new KillAllCommand());
        }
    }

}
