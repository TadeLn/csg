package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NotEnoughArgumentsException;
import tadeln.csg.command.exceptions.TileOutOfReachException;

public class SetSelectionCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        if (super.getCommandParts().size() <= 3) {
            throw new NotEnoughArgumentsException(3, super.getCommandParts().size());
        }
        Vector3 position = new Vector3();
        try {
            position.setX(Integer.parseInt(super.getPart(1)));
        } catch (NumberFormatException e) {
            throw new NotAnIntegerException(super.getPart(1));
        }
        try {
            position.setY(Integer.parseInt(super.getPart(2)));
        } catch (NumberFormatException e) {
            throw new NotAnIntegerException(super.getPart(2));
        }
        try {
            position.setZ(Integer.parseInt(super.getPart(3)));
        } catch (NumberFormatException e) {
            throw new NotAnIntegerException(super.getPart(3));
        }
        if (Utils.isInRange(position.getX(), -2, 2) &&
                Utils.isInRange(position.getY(), -2, 2) &&
                Utils.isInRange(position.getZ(), -1, 0)
        ) {
            game.setSelection(position);
        } else {
            throw new TileOutOfReachException(position);
        }
    }

    public SetSelectionCommand() {
        super(false, true);
        aliases.add("setsel");
        shortDescription = "Set the selected block with 3 integer parameters";
    }

}
