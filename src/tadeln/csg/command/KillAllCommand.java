package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.world.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class KillAllCommand extends Command {

    public void run(Game game) {
        List<Entity> entities = new ArrayList<Entity>(game.getWorld().getEntities());
        for (int i = 0; i < entities.size(); i++) {
            if (!(entities.get(i) instanceof Player)) {
                entities.get(i).die(game.getWorld());
            }
        }
    }

    public KillAllCommand() {
        super(true, true);
        aliases.add("killall");
        shortDescription = "Kill all entities apart from the player";
    }

}