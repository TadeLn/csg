package tadeln.csg.command;

import tadeln.csg.Game;

public class ToggleDebugCommand extends Command {

    public void run(Game game) {
        if (Game.isDebugVisible()) {
            game.setDebugVisible(false);
        } else {
            game.setDebugVisible(true);
        }
    }

    public ToggleDebugCommand() {
        super(false, true);
        aliases.add("debug");
        shortDescription = "Toggle debug info";
    }

}
