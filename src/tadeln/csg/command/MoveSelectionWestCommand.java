package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSelectionWestCommand extends MoveSelectionCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(-1, 0, 0));
    }

    public MoveSelectionWestCommand() {
        aliases.add("j");
        shortDescription = "Move the selection west";
    }
}
