package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveEastCommand extends MoveCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(1, 0, 0));
    }

    public MoveEastCommand() {
        aliases.add("d");
        aliases.add("east");
        shortDescription = "Move east";
    }

}
