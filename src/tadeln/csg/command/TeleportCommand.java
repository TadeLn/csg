package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NotEnoughArgumentsException;

public class TeleportCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        if (super.getCommandParts().size() <= 2) {
            throw new NotEnoughArgumentsException(2, super.getCommandParts().size());
        }

        Vector3 position = new Vector3(0, 0, game.getWorld().getSize().getZ());

        if (Utils.isInt(super.getPart(1))) {
            position.setX(Utils.tryParseInt(super.getPart(1)));
        } else {
            throw new NotAnIntegerException(super.getPart(1));
        }
        if (Utils.isInt(super.getPart(2))) {
            position.setY(Utils.tryParseInt(super.getPart(2)));
        } else {
            throw new NotAnIntegerException(super.getPart(2));
        }

        game.getPlayer().setPosition(position);
        game.getPlayer().forceFall(game.getWorld());
    }

    public TeleportCommand() {
        super(true, true);
        aliases.add("tp");
        aliases.add("teleport");
        shortDescription = "Teleport to a given location on the map";
    }

}
