package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.Vector3;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.world.tile.Tile;

import java.util.List;

public class ExamineCommand extends Command {

    public void run(Game game) {
        Vector3 location = game.getSelectedTileLocation();
        Tile tile = game.getWorld().getTile(location);

        Renderer.print(new FormattedStringChain(
                new FormattedString("\nTILE", new Formatting().color(Color.BRIGHT_WHITE).underline()))
                .append(new FormattedString(":\n").color(Color.BRIGHT_WHITE))
        );
        Renderer.print(new FormattedStringChain(tile.getFullName()).append("\n" +
                "     ").append(new FormattedString(tile.getDescription(), Formatting.WHITE)).append("\n"));

        List<Entity> entities = game.getWorld().getEntitiesAtLocation(location);
        if (entities.size() > 0) {
            Renderer.print(new FormattedStringChain(
                    new FormattedString("\nENTITIES", new Formatting().color(Color.BRIGHT_WHITE).underline()))
                    .append(new FormattedString(":\n").color(Color.BRIGHT_WHITE))
            );
            for (Entity e : entities) {
                Renderer.print(new FormattedStringChain(e.getFullName()).append("\n" +
                        "     ").append(new FormattedString(e.getDescription(), Formatting.WHITE)).append("\n"));
            }
        }
    }

    public ExamineCommand() {
        super(false, false);
        aliases.add("e");
        aliases.add("examine");
        shortDescription = "Show info about the selected tile";
    }

}
