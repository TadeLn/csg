package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSouthCommand extends MoveCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(0, 1, 0));
    }

    public MoveSouthCommand() {
        aliases.add("s");
        aliases.add("south");
        shortDescription = "Move south";
    }

}
