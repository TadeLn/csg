package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.Utils;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NoSuchPageException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

import java.util.ArrayList;
import java.util.List;

public class HelpCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        List<Command> cmdRegistry = game.getCmdManager().getCommandRegistry();

        int startingLine;
        if (super.getCommandParts().size() <= 1) {
            startingLine = -1;
        } else {
            if (Utils.isInt(super.getPart(1))) {
                startingLine = (Utils.tryParseInt(super.getPart(1)) - 1) * 10;
            } else {
                throw new NotAnIntegerException(super.getPart(1));
            }
        }

        int endingLine = Math.min(startingLine + 10, cmdRegistry.size());

        if (startingLine == -1) {
            startingLine = 0;
            endingLine = cmdRegistry.size();
        }

        if (startingLine > cmdRegistry.size()) {
            throw new NoSuchPageException((startingLine / 10) + 1);
        }

        List<String> lines = new ArrayList<>();
        for (int i = startingLine; i < endingLine; i++) {
            Command cmd = cmdRegistry.get(i);
            String line = "";
            for (int j = 0; j < cmd.getAliases().size(); j++) {
                String alias = cmd.getAliases().get(j);
                line += alias + " ";
            }
            lines.add(line);
        }

        int max = 0;
        for (String line : lines) {
            if (line.length() > max) {
                max = line.length();
            }
        }

        for (int i = startingLine; i < endingLine; i++) {
            Command cmd = game.getCmdManager().getCommandRegistry().get(i);
            Renderer.print(new FormattedString(Utils.paddingRight(lines.get(i - startingLine), max, ' '), new Formatting(Color.BRIGHT_WHITE).bold()));
            Renderer.print("- " + cmd.getShortDescription() + "\n");
        }
    }

    public HelpCommand() {
        super(false, false);
        aliases.add("help");
        shortDescription = "Show this message";
    }

}
