package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSelectionDownCommand extends MoveSelectionCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(0, 0, -1));
    }

    public MoveSelectionDownCommand() {
        aliases.add("-");
        shortDescription = "Move the selection down a layer";
    }
}
