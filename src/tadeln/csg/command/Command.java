package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.command.exceptions.InvalidCommandException;

import java.util.ArrayList;
import java.util.List;

public abstract class Command {

    protected List<String> aliases;

    public List<String> getAliases() {
        return aliases;
    }

    protected boolean shouldTick = true;

    protected boolean shouldDraw = true;

    protected String shortDescription = "No description provided";

    protected List<String> commandParts;

    public List<String> getCommandParts() {
        return commandParts;
    }

    public void setCommandParts(List<String> commandParts) {
        this.commandParts = commandParts;
    }

    public String getPart(int i) {
        if (commandParts.size() > i) {
            return commandParts.get(i);
        } else {
            return null;
        }
    }

    public boolean shouldTick() {
        return shouldTick;
    }

    public boolean shouldDraw() {
        return shouldDraw;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void run(Game game) throws InvalidCommandException {
    }

    public Command(boolean shouldTick, boolean shouldDraw) {
        this(new ArrayList<String>());
        this.shouldTick = shouldTick;
        this.shouldDraw = shouldDraw;
    }

    public Command(List<String> commandParts) {
        this.aliases = new ArrayList<String>();
        this.commandParts = commandParts;
    }

    public Command(Command command) {
        this(command.commandParts);
    }

}
