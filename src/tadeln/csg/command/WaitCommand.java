package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.Utils;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NumberOutOfRange;

public class WaitCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        int ticks = 1;
        if (super.getCommandParts().size() >= 2) {
            String stringInteger = super.getPart(1);
            if (!Utils.isInt(stringInteger)) throw new NotAnIntegerException(stringInteger);
            ticks = Utils.tryParseInt(stringInteger);

            if (!Utils.isInRange(ticks, 1, 100)) throw new NumberOutOfRange(ticks, 1, 100);
        }
        for (int i = 0; i < ticks-1; i++) {
            long prevTime = System.currentTimeMillis();
            Renderer.printOnLoop("Waiting...");
            game.tick();
            game.drawFrame();
            try {
                Thread.sleep(Math.max(0, 50 - (System.currentTimeMillis() - prevTime)));
            } catch (InterruptedException e) {
                Renderer.printOnLoop("Interrupted");
            }
        }
    }

    public WaitCommand() {
        super(true, true);
        aliases.add("wait");
        shortDescription = "Wait a given number of ticks";
    }

}
