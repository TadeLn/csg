package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Utils;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.world.entity.Pig;

public class SpawnCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        int count = 1;

        if (super.getCommandParts().size() >= 2) {
            if (Utils.isInt(super.getPart(1))) {
                count = Utils.tryParseInt(super.getPart(1));
            } else {
                throw new NotAnIntegerException(super.getPart(1));
            }
        }
        for (int i = 0; i < count; i++) {
            Entity e = new Pig();
            e.setPosition(game.getPlayer().getPosition());
            game.getWorld().spawnEntity(e, true);
        }
    }

    public SpawnCommand() {
        super(false, true);
        aliases.add("spawn");
        shortDescription = "Spawn a mob";
    }

}
