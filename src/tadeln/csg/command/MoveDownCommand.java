package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveDownCommand extends MoveCommand {

    public void run(Game game) throws InvalidCommandException {
        Vector3 position = game.getWorld().getPlayer().getPosition();
        if (game.getWorld().getTile(position.add(new Vector3(0, 0, -1))).isSemisolid()) {
            super.run(game, new Vector3(0, 0, -1));
        }
    }

    public MoveDownCommand() {
        aliases.add("down");
        shortDescription = "Move down a climbable tile";
    }

}
