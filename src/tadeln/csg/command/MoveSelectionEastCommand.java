package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSelectionEastCommand extends MoveSelectionCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(1, 0, 0));
    }

    public MoveSelectionEastCommand() {
        aliases.add("l");
        shortDescription = "Move the selection east";
    }
}
