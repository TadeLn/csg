package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.world.item.Inventory;
import tadeln.csg.world.item.ItemStack;

public class InventoryCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        Inventory inventory = game.getPlayer().getInventory();
        for (int i = 0; i < inventory.getSlots().size(); i++) {
            ItemStack stack = inventory.getSlots().get(i);
            Renderer.print(
                    new FormattedStringChain("" + (i + 1) + ": ")
                            .append(stack.getFullName())
                            .append("\n")
            );
        }
    }

    public InventoryCommand() {
        super(false, false);
        aliases.add("inv");
        aliases.add("inventory");
        shortDescription = "Show your inventory";
    }

}
