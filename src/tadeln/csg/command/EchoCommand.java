package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;

public class EchoCommand extends Command {

    public void run(Game game) {
        Renderer.printOnLoop(new FormattedString(
                super.getPart(1),
                new Formatting(Color.WHITE)
        ));
    }

    public EchoCommand() {
        super(false, true);
        aliases.add("echo");
        shortDescription = "Print given string to the screen";
    }

}
