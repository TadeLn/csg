package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NoEntitySelected;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.world.entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class PickUpCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        List<Entity> entities = new ArrayList<Entity>();
        for (Entity e : game.getWorld().getEntitiesAtLocation(game.getSelectedTileLocation())) {
            if (e instanceof ItemEntity) {
                entities.add(e);
            }
        }
        ItemEntity entity = (ItemEntity) game.queryEntity(entities);
        if (entity == null) {
            throw new NoEntitySelected();
        }
        entity.setDespawned();

        int size = entity.getInventory().getSlots().size();
        if (size == 0) {
            Renderer.printOnLoop("...didn't pick up anything");
        } else if (size == 1) {
            Renderer.printOnLoop("Picked up " + entity.getInventory().getSlots().get(0).getFullName(true));
        } else {
            Renderer.printOnLoop("Picked up multiple items");
        }
        game.getPlayer().getInventory().add(entity.getInventory());
    }

    public PickUpCommand() {
        super(true, true);
        aliases.add("p");
        aliases.add("pickup");
        shortDescription = "Pick up an item from the ground";
    }

}
