package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.Settings;
import tadeln.csg.formatting.Color;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.Utils;

public class ColorsCommand extends Command {

    public void run(Game game) {
        String colorMode;
        if (Settings.useColor) {
            if (Settings.extraColors) {
                colorMode = "256 colors";
            } else {
                colorMode = "16 colors";
            }
            if (Settings.reverseBlackWhite) {
                colorMode += ", black and white reversed";
            }
        } else {
            colorMode = "Monochrome";
        }
        Renderer.print(new FormattedString("Current color mode: " + colorMode + "\n\n"));

        if (Settings.useColor) {
            Renderer.print(new FormattedString("16 colors:\n"));

            Renderer.print(new FormattedString("Normal: "));
            Renderer.print(new FormattedString("BLACK   ", new Formatting(Color.BLACK)));
            Renderer.print(new FormattedString("RED     ", new Formatting(Color.RED)));
            Renderer.print(new FormattedString("GREEN   ", new Formatting(Color.GREEN)));
            Renderer.print(new FormattedString("YELLOW  ", new Formatting(Color.YELLOW)));
            Renderer.print(new FormattedString("BLUE    ", new Formatting(Color.BLUE)));
            Renderer.print(new FormattedString("PURPLE  ", new Formatting(Color.BLUE)));
            Renderer.print(new FormattedString("CYAN    ", new Formatting(Color.CYAN)));
            Renderer.print(new FormattedString("WHITE   ", new Formatting(Color.WHITE)));
            Renderer.print(new FormattedString("\n"));

            Renderer.print(new FormattedString("Bright: "));
            Renderer.print(new FormattedString("BLACK   ", new Formatting(Color.BRIGHT_BLACK)));
            Renderer.print(new FormattedString("RED     ", new Formatting(Color.BRIGHT_RED)));
            Renderer.print(new FormattedString("GREEN   ", new Formatting(Color.BRIGHT_GREEN)));
            Renderer.print(new FormattedString("YELLOW  ", new Formatting(Color.BRIGHT_YELLOW)));
            Renderer.print(new FormattedString("BLUE    ", new Formatting(Color.BRIGHT_BLUE)));
            Renderer.print(new FormattedString("PURPLE  ", new Formatting(Color.BRIGHT_BLUE)));
            Renderer.print(new FormattedString("CYAN    ", new Formatting(Color.BRIGHT_CYAN)));
            Renderer.print(new FormattedString("WHITE   ", new Formatting(Color.BRIGHT_WHITE)));
            Renderer.print(new FormattedString("\n"));

            Renderer.print(new FormattedString("Normal: "));
            Renderer.print(new FormattedString("BLACK   ", new Formatting().background(Color.BLACK)));
            Renderer.print(new FormattedString("RED     ", new Formatting().background(Color.RED)));
            Renderer.print(new FormattedString("GREEN   ", new Formatting().background(Color.GREEN)));
            Renderer.print(new FormattedString("YELLOW  ", new Formatting().background(Color.YELLOW)));
            Renderer.print(new FormattedString("BLUE    ", new Formatting().background(Color.BLUE)));
            Renderer.print(new FormattedString("PURPLE  ", new Formatting().background(Color.PURPLE)));
            Renderer.print(new FormattedString("CYAN    ", new Formatting().background(Color.CYAN)));
            Renderer.print(new FormattedString("WHITE   ", new Formatting().background(Color.WHITE)));
            Renderer.print(new FormattedString("\n"));

            Renderer.print(new FormattedString("Bright: "));
            Renderer.print(new FormattedString("BLACK   ", new Formatting().background(Color.BRIGHT_BLACK)));
            Renderer.print(new FormattedString("RED     ", new Formatting().background(Color.BRIGHT_RED)));
            Renderer.print(new FormattedString("GREEN   ", new Formatting().background(Color.BRIGHT_GREEN)));
            Renderer.print(new FormattedString("YELLOW  ", new Formatting().background(Color.BRIGHT_YELLOW)));
            Renderer.print(new FormattedString("BLUE    ", new Formatting().background(Color.BRIGHT_BLUE)));
            Renderer.print(new FormattedString("PURPLE  ", new Formatting().background(Color.BRIGHT_PURPLE)));
            Renderer.print(new FormattedString("CYAN    ", new Formatting().background(Color.BRIGHT_CYAN)));
            Renderer.print(new FormattedString("WHITE   ", new Formatting().background(Color.BRIGHT_WHITE)));
            Renderer.print(new FormattedString("\n"));

            if (Settings.extraColors) {
                Renderer.print(new FormattedString("\n"));
                Renderer.print(new FormattedString("256 colors: \n"));

                final int firstLineWidth = 8;
                final int firstLines = 2;
                final int lineWidth = 12;

                int colorNumber = 0;

                for (int i = 0; i < firstLines; i++) {
                    for (int j = 0; j < firstLineWidth; j++) {
                        String colorString = "" + colorNumber;
                        colorString = Utils.center(colorString, 8);
                        Renderer.print(new FormattedString(colorString, new Formatting().background(colorNumber)));
                        colorNumber++;
                    }
                    Renderer.print(new FormattedString("\n"));
                }

                for (int i = 0; i < (256 - (firstLines * 8)) / lineWidth; i++) {
                    for (int j = 0; j < lineWidth; j++) {
                        String colorString = "" + colorNumber;
                        colorString = Utils.center(colorString, 8);
                        Renderer.print(new FormattedString(colorString, new Formatting().background(colorNumber)));
                        colorNumber++;
                    }
                    Renderer.print(new FormattedString("\n"));
                }
            }
        }
    }

    public ColorsCommand() {
        super(false, false);
        aliases.add("colortest");
        shortDescription = "Show colors";
    }

}
