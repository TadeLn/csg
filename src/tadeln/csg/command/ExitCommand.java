package tadeln.csg.command;

import tadeln.csg.Game;

public class ExitCommand extends Command {

    public void run(Game game) {
        game.stop();
    }

    public ExitCommand() {
        super(false, false);
        aliases.add("exit");
        shortDescription = "Close the game";
    }

}
