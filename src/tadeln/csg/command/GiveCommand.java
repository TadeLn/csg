package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Utils;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NotEnoughArgumentsException;
import tadeln.csg.command.exceptions.NumberOutOfRange;
import tadeln.csg.world.World;
import tadeln.csg.world.item.ItemStack;

public class GiveCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        if (super.getCommandParts().size() <= 1) {
            throw new NotEnoughArgumentsException(1, super.getCommandParts().size());
        }
        if (!Utils.isInt(super.getPart(1))) {
            throw new NotAnIntegerException(super.getPart(1));
        }
        int id = Utils.tryParseInt(super.getPart(1));
        if (!Utils.isInRange(id, 0, World.getItemRegistry().size()-1)) {
            throw new NumberOutOfRange(id, 0, World.getItemRegistry().size()-1);
        }

        ItemStack stack = new ItemStack((byte) id);

        if (super.getCommandParts().size() >= 2) {
            if (!Utils.isInt(super.getPart(2))) {
                throw new NotAnIntegerException(super.getPart(2));
            }
            int count = Utils.tryParseInt(super.getPart(2));
            if (!Utils.isInRange(count, 1, stack.getItem().getStackSize())) {
                throw new NumberOutOfRange(count, 1, stack.getItem().getStackSize());
            }
            stack.setCount(count);
        }

        game.getPlayer().getInventory().add(stack);
    }

    public GiveCommand() {
        super(false, true);
        aliases.add("give");
        shortDescription = "Give yourself an item";
    }

}
