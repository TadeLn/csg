package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.CantClimbException;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveUpCommand extends MoveCommand {

    public void run(Game game) throws InvalidCommandException {
        Vector3 position = game.getPlayer().getPosition();
        if (game.getWorld().getTile(position).isSemisolid()) {
            super.run(game, new Vector3(0, 0, 1));
        } else {
            throw new CantClimbException(game.getWorld().getTile(position));
        }
    }

    public MoveUpCommand() {
        aliases.add("up");
        shortDescription = "Move up a climbable object";
    }

}
