package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.TileOutOfReachException;

public abstract class MoveSelectionCommand extends Command {

    public void run(Game game, Vector3 offset) throws InvalidCommandException {
        Vector3 newSelection = game.getSelection().add(offset);
        if (Utils.isInRange(newSelection.getX(), -2, 2) &&
                Utils.isInRange(newSelection.getY(), -2, 2) &&
                Utils.isInRange(newSelection.getZ(), -1, 0)
        ) {
            game.setSelection(newSelection);
        } else {
            throw new TileOutOfReachException(newSelection);
        }
    }

    public MoveSelectionCommand() {
        super(false, true);
    }

}
