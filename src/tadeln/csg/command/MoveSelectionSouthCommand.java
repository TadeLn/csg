package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSelectionSouthCommand extends MoveSelectionCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(0, 1, 0));
    }

    public MoveSelectionSouthCommand() {
        aliases.add("k");
        shortDescription = "Move the selection south";
    }
}
