package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;

public class TestCommand extends Command {

    public void run(Game game) {
        Renderer.print(game.getWorld().getPlayer().getPosition().toString());
    }

    public TestCommand() {
        super(false, false);
        aliases.add("test");
        shortDescription = "Test command";
    }

}
