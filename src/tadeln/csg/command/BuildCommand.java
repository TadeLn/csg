package tadeln.csg.command;

import tadeln.csg.Renderer;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.*;
import tadeln.csg.world.World;
import tadeln.csg.Game;

public class BuildCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        Vector3 location = game.getSelectedTileLocation();
        Vector3 worldSize = game.getWorld().getSize();
        if (Utils.isInRange(location.getX(), 0, worldSize.getX()-1) &&
                Utils.isInRange(location.getY(), 0, worldSize.getY()-1) &&
                Utils.isInRange(location.getZ(), 0, worldSize.getZ()-1)
        ) {
            if (game.getWorld().getEntitiesAtLocation(location).size() == 0 &&
                    game.getWorld().getTileId(location) == 0
            ) {
                if (super.getCommandParts().size() <= 1) {
                    throw new NotEnoughArgumentsException();
                }
                if (!Utils.isInt(super.getPart(1))) {
                    throw new NotAnIntegerException(super.getPart(1));
                }
                byte tileId = (byte) Utils.tryParseInt(super.getPart(1));
                if (tileId >= World.getTileRegistry().size()) {
                    throw new InvalidTileIdException(tileId);
                } else {
                    Renderer.printOnLoop("Built " + World.getTileRegistry().get(tileId).getFullName());
                    game.getWorld().setTileId(location, tileId);
                }
            } else {
                throw new PositionOccupiedException(location);
            }
        } else {
            throw new PositionOutOfBoundsException(location);
        }
    }

    public BuildCommand() {
        super(true, true);
        aliases.add("b");
        aliases.add("build");
        shortDescription = "Place a tile in the selection";
    }

}
