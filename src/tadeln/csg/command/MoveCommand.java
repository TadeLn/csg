package tadeln.csg.command;

import tadeln.csg.Renderer;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NumberOutOfRange;
import tadeln.csg.Game;

public abstract class MoveCommand extends Command {

    public void run(Game game, Vector3 offset) throws InvalidCommandException {
        int steps = 1;
        if (super.getCommandParts().size() >= 2) {
            String stringInteger = super.getPart(1);
            if (!Utils.isInt(stringInteger)) throw new NotAnIntegerException(stringInteger);
            steps = Utils.tryParseInt(stringInteger);

            if (!Utils.isInRange(steps, 1, 20)) throw new NumberOutOfRange(steps, 1, 10);
        }

        game.getPlayer().move(new Vector3(offset), game.getWorld());
        if (game.getWorld().getTile(game.getPlayer().getPosition()).isLiquid()) return;
        for (int i = 0; i < steps-1; i++) {
            long prevTime = System.currentTimeMillis();

            game.tick();
            game.drawFrame();

            game.getPlayer().move(new Vector3(offset), game.getWorld());
            if (game.getWorld().getTile(game.getPlayer().getPosition()).isLiquid()) return;

            if (i > 0) {
                try {
                    Thread.sleep(Math.max(0, 50 - (System.currentTimeMillis() - prevTime)));
                } catch (InterruptedException e) {
                    Renderer.printOnLoop("InterruptedException");
                }
            }
        }
    }

    public MoveCommand() {
        super(true, true);
    }

}
