package tadeln.csg.command;

import tadeln.csg.Game;

public class RandomTeleportCommand extends Command {

    public void run(Game game) {
        game.getPlayer().randomTeleport(game.getWorld());
    }

    public RandomTeleportCommand() {
        super(true, true);
        aliases.add("rtp");
        aliases.add("randomtp");
        shortDescription = "Teleport to a random location on the map";
    }

}
