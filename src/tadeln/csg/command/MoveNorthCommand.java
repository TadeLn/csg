package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveNorthCommand extends MoveCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(0, -1, 0));
    }

    public MoveNorthCommand() {
        aliases.add("w");
        aliases.add("north");
        shortDescription = "Move north";
    }

}
