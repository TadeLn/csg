package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveWestCommand extends MoveCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(-1, 0, 0));
    }

    public MoveWestCommand() {
        aliases.add("a");
        aliases.add("west");
        shortDescription = "Move west";
    }

}
