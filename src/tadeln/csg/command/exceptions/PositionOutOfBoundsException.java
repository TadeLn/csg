package tadeln.csg.command.exceptions;

import tadeln.csg.Vector3;

public class PositionOutOfBoundsException extends InvalidCommandException {
    public PositionOutOfBoundsException(Vector3 position) {
        super("Tile at position " + position.toString() + " is out of bounds of the world");
    }
}
