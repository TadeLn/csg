package tadeln.csg.command.exceptions;

import tadeln.csg.world.tile.Tile;

public class CantClimbException extends InvalidCommandException {
    public CantClimbException(Tile tile) {
        super("You can't climb on " + tile.getName().toString());
    }
}
