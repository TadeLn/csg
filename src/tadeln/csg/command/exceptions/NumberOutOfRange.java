package tadeln.csg.command.exceptions;

public class NumberOutOfRange extends InvalidCommandException {
    public NumberOutOfRange(int input, int min, int max) {
        super("The number " + input + " is out of range <" + min + ", " + max + ">");
    }
}
