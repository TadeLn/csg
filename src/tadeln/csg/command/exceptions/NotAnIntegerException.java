package tadeln.csg.command.exceptions;

public class NotAnIntegerException extends InvalidCommandException {
    public NotAnIntegerException(String input) {
        super("The string \"" + input + "\" could not be converted into an integer");
    }
}
