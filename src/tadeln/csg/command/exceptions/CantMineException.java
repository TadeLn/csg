package tadeln.csg.command.exceptions;

import tadeln.csg.world.tile.Tile;

public class CantMineException extends InvalidCommandException {
    public CantMineException(Tile tile) {
        super("You can't mine " + tile.getName().toString());
    }
}
