package tadeln.csg.command.exceptions;

public class NoSuchPageException extends InvalidCommandException {
    public NoSuchPageException(int page) {
        super("Page " + page + " does not exist");
    }
}
