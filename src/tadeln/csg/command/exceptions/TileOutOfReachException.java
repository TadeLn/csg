package tadeln.csg.command.exceptions;

import tadeln.csg.Vector3;

public class TileOutOfReachException extends InvalidCommandException {
    public TileOutOfReachException(Vector3 position) {
        super("Tile at position " + position.toString() + " is out of reach");
    }
}
