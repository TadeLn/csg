package tadeln.csg.command.exceptions;

import tadeln.csg.Vector3;

public class PositionOccupiedException extends InvalidCommandException {
    public PositionOccupiedException(Vector3 location) {
        super("Tile at loaction " + location.toString() + " is occupied");
    }
}
