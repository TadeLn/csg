package tadeln.csg.command.exceptions;

public class MissingQuoteEndException extends InvalidCommandException {
    private String message;

    public String getMessage() {
        return message;
    }

    public MissingQuoteEndException() {
        message = "Expected \" at the end";
    }

    public MissingQuoteEndException(String message) {
        this.message = message;
    }

}
