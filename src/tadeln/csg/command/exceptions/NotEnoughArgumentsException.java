package tadeln.csg.command.exceptions;

public class NotEnoughArgumentsException extends InvalidCommandException {
    public NotEnoughArgumentsException() {
        super("Not enough arguments");
    }

    public NotEnoughArgumentsException(int expected, int got) {
        super("Not enough arguments (expected " + (expected) + ", got only " + (got - 1) + ")");
    }
}
