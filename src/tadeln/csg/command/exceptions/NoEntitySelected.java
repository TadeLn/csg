package tadeln.csg.command.exceptions;

public class NoEntitySelected extends InvalidCommandException {
    public NoEntitySelected() {
        super("No entity selected");
    }
}
