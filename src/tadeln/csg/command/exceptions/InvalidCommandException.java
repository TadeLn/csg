package tadeln.csg.command.exceptions;

public class InvalidCommandException extends Exception {

    private String message;

    public String getMessage() {
        return message;
    }

    public InvalidCommandException() {
        message = "Unknown error";
    }

    public InvalidCommandException(String message) {
        this.message = message;
    }
}
