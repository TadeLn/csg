package tadeln.csg.command.exceptions;

public class InvalidTileIdException extends InvalidCommandException {
    public InvalidTileIdException(byte tileId) {
        super("Tile with ID #" + tileId + " does not exist");
    }
}
