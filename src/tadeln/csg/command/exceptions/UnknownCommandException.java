package tadeln.csg.command.exceptions;

public class UnknownCommandException extends InvalidCommandException {
    public UnknownCommandException(String commandName) {
        super("Unknown Command: " + commandName);
    }
}