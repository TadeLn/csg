package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.InvalidCommandException;

public class MoveSelectionUpCommand extends MoveSelectionCommand {

    public void run(Game game) throws InvalidCommandException {
        super.run(game, new Vector3(0, 0, 1));
    }

    public MoveSelectionUpCommand() {
        aliases.add("=");
        shortDescription = "Move the selection up a layer";
    }
}
