package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.Utils;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.NotAnIntegerException;
import tadeln.csg.command.exceptions.NumberOutOfRange;

public class RestCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        int ticks = 1;
        if (super.getCommandParts().size() >= 2) {
            String stringInteger = super.getPart(1);
            if (!Utils.isInt(stringInteger)) throw new NotAnIntegerException(stringInteger);
            ticks = Utils.tryParseInt(stringInteger);

            if (!Utils.isInRange(ticks, 100, 500)) throw new NumberOutOfRange(ticks, 100, 500);
        }
        Renderer.print("\n".repeat(100));
        for (int i = 0; i < ticks; i++) {
            if (i % 100 == 0) Renderer.print("Resting...\n");
            Renderer.render();
            game.tick();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                Renderer.printOnLoop("Interrupted");
            }
        }
    }

    public RestCommand() {
        super(false, true);
        aliases.add("rest");
        shortDescription = "Sleep for a given number of ticks";
    }

}
