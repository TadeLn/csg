package tadeln.csg.command;

import tadeln.csg.Game;

public class SaveCommand extends Command {

    public void run(Game game) {
        game.getWorld().save();
    }

    public SaveCommand() {
        super(false, true);
        aliases.add("save");
        shortDescription = "Save the world to a file";
    }

}
