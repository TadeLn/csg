package tadeln.csg.command;

import tadeln.csg.Game;
import tadeln.csg.Renderer;
import tadeln.csg.Utils;
import tadeln.csg.Vector3;
import tadeln.csg.command.exceptions.CantMineException;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.command.exceptions.PositionOutOfBoundsException;
import tadeln.csg.world.tile.AirTile;
import tadeln.csg.world.tile.Tile;

public class MineCommand extends Command {

    public void run(Game game) throws InvalidCommandException {
        Vector3 location = game.getSelectedTileLocation();
        Vector3 worldSize = game.getWorld().getSize();
        if (Utils.isInRange(location.getX(), 0, worldSize.getX()-1) &&
                Utils.isInRange(location.getY(), 0, worldSize.getY()-1) &&
                Utils.isInRange(location.getZ(), 0, worldSize.getZ()-1)
        ) {
            Tile tile = game.getWorld().getTile(location);
            if (!tile.isSolid() || tile.isUnbreakable()) {
                throw new CantMineException(tile);
            } else {
                Renderer.printOnLoop("Mined " + tile.getFullName());
                game.getWorld().setTileId(location, AirTile.getIdStatic());
            }
        } else {
            throw new PositionOutOfBoundsException(location);
        }
    }

    public MineCommand() {
        super(true, true);
        aliases.add("m");
        aliases.add("mine");
        shortDescription = "Break the selected tile";
    }

}
