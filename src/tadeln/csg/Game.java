package tadeln.csg;

import tadeln.csg.formatting.Color;
import tadeln.csg.command.Command;
import tadeln.csg.command.manager.CommandManager;
import tadeln.csg.formatting.FormattedString;
import tadeln.csg.formatting.FormattedStringChain;
import tadeln.csg.formatting.Formatting;
import tadeln.csg.command.exceptions.InvalidCommandException;
import tadeln.csg.world.World;
import tadeln.csg.world.entity.Entity;
import tadeln.csg.world.entity.Player;
import tadeln.csg.world.gen.WorldGen;

import java.util.List;
import java.util.Scanner;

public class Game {

    private boolean running;

    private static boolean debugVisible;

    private static String nickname;

    private Scanner scanner;

    private CommandManager cmdManager;

    private Command lastCommand;

    private World world;

    private Vector3 selection;

    public boolean isRunning() {
        return running;
    }

    public static boolean isDebugVisible() {
        return debugVisible;
    }

    public static String getNickname() {
        return nickname;
    }

    public CommandManager getCmdManager() {
        return cmdManager;
    }

    public World getWorld() {
        return world;
    }

    public Player getPlayer() { return world.getPlayer(); }

    public Vector3 getSelection() {
        return selection;
    }

    public void setDebugVisible(boolean debugVisible) {
        Game.debugVisible = debugVisible;
    }

    public void setSelection(Vector3 location) {
        selection = new Vector3(location);
    }

    public Vector3 getSelectedTileLocation() {
        return new Vector3(selection).add(getPlayer().getPosition());
    }

    public Entity queryEntity(List<Entity> entities) {
        if (entities.size() > 1) {
            String input;
            boolean chosen = false;
            int number = -1;
            while (!chosen) {
                Renderer.print("Which entity do you want to perform the action on?\n" +
                            "[0] Cancel\n");

                for (int i = 0; i < entities.size(); i++) {
                    Entity e = entities.get(i);
                    Renderer.print(new FormattedStringChain("[" + (i + 1) + "] ").append(e.getFullName()).append("\n" +
                            "     ").append(new FormattedString(e.getDescription(), Formatting.WHITE)).append("\n"));
                }
                Renderer.print("\n? ");
                Renderer.render();

                input = scanner.nextLine();
                if (Utils.isInt(input)) {
                    number = Utils.tryParseInt(input);
                    if (Utils.isInRange(number, 0, entities.size())) {
                        chosen = true;
                        number--;
                    }
                }
            }
            if (number == -1) {
                return null;
            } else {
                return entities.get(number);
            }
        } else if (entities.size() > 0) {
            return entities.get(0);
        } else {
            return null;
        }
    }

    public void start(String[] args, String nickname) throws Exception {
        running = true;

        Game.nickname = nickname;

        Settings.loadFromFile();

        Renderer.print("Loading settings from args...\n");
        Settings.loadFromArgs(args);

        debugVisible = Settings.debug;
        Debug.clear();

        cmdManager = new CommandManager();

        Renderer.print("Generating a new world...\n");

        world = new World(new Vector3(Settings.worldSizeX, Settings.worldSizeY, Settings.worldSizeZ));
        WorldGen.generate(world);
        world.spawnPlayer(getNickname());

        Renderer.print("Done\n");

        while (running) {
            loop();
        }
    }

    public void stop() {
        running = false;
    }

    public void tick() {
        // Update world
        long prevTime = System.currentTimeMillis();
        world.tick();
        long mspt = (System.currentTimeMillis() - prevTime);
        Debug.add("TPS: " + (1000 / Math.max(1, mspt)) + " MSPT: " + mspt + "ms\n");
    }

    public void drawFrame() {
        drawFrame(true);
    }

    public void drawFrame(boolean render) {
        long prevTime = System.currentTimeMillis();
        // Set camera
        Rect camera = new Rect(new Vector2(), new Vector2(Settings.windowWidth, Settings.windowHeight));
        camera.setCenter(new Vector2(getPlayer().getPosition()));

        // Draw world
        world.draw(camera, new Vector2(getSelectedTileLocation()));

        // Print selected tile info
        Renderer.print(new FormattedStringChain("Selected tile: ")
                .append(world.getTile(getSelectedTileLocation()).getFullName())
                .append(" @ " + getSelectedTileLocation() + " ~ " + getSelection())
                .append(" | Entities: " + world.getEntitiesAtLocation(getSelectedTileLocation()).size())
                .append("\n")
        );

        long mspf = (System.currentTimeMillis() - prevTime);
        Debug.add("FPS: " + (1000 / Math.max(1, mspf)) + " MSPF: " + mspf + "ms\n");

        if (isDebugVisible()) {
            Renderer.print("\n");
            Renderer.print(Debug.get());
        }
        Debug.clear();

        Renderer.loop();
        Renderer.print("\n");

        if (render) {
            Renderer.print("\n");
            Renderer.render();
        }
    }

    public void prompt() {
        // Prompt
        Renderer.print(new FormattedStringChain()
                .append(new FormattedString("[T: " + world.getAge() + "] ", new Formatting(Color.WHITE)))
                .append(new FormattedString(getPlayer().getPosition().toString() + " ", new Formatting(Color.WHITE)))
                .append(new FormattedString("HP: " + getPlayer().getHealth() + "/" + getPlayer().getMaxHealth() + " ", new Formatting(Color.RED).bold()))
                .append(new FormattedString(getPlayer().getNickname() + " > ", new Formatting(Color.BRIGHT_YELLOW).bold()))
        );
        Renderer.print("\u001b[s\n\u001b[u\u001b[1A");
        Renderer.render();
        String input = scanner.nextLine();

        lastCommand = null;
        try {
            lastCommand = cmdManager.parseCommand(input);
            if (lastCommand != null) {
                lastCommand.run(this);
            }
        } catch (InvalidCommandException e) {
            Renderer.printOnLoop(new FormattedString(e.getMessage(), Formatting.RED));
            lastCommand = null;
        }
        Renderer.print(new FormattedString("\n"));
    }

    public void loop() {
        if (lastCommand != null) {
            if (lastCommand.shouldTick()) {
                tick();
            }
            if (lastCommand.shouldDraw()) {
                drawFrame(false);
            }
        } else {
            drawFrame(false);
        }

        // Ask for the next command
        prompt();
    }

    public Game() {
        running = false;
        scanner = new Scanner(System.in);
        selection = new Vector3(0, 0, 0);
    }
}