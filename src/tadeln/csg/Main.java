package tadeln.csg;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        Game game = new Game();
        System.out.println("Console Sandbox Game by TadeLn");
        System.out.println("https://tadeln.stary.pc.pl");
        System.out.println();
        System.out.print("Enter nickname: ");
        String nickname = scanner.nextLine();
        game.start(args, nickname);
    }
}