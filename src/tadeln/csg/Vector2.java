package tadeln.csg;

public class Vector2 {

    private int x;

    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Vector2 add(Vector2 vector) {
        Vector2 result = new Vector2(this);
        result.setX(result.getX() + vector.getX());
        result.setY(result.getY() + vector.getY());
        return result;
    }

    public Vector2 subtract(Vector2 vector) {
        Vector2 result = new Vector2(this);
        result.setX(result.getX() - vector.getX());
        result.setY(result.getY() - vector.getY());
        return result;
    }

    public Vector2 invert() {
        Vector2 result = new Vector2(this);
        result.setX(-result.getX());
        result.setY(-result.getY());
        return result;
    }

    public Vector2 multiply(int number) {
        Vector2 result = new Vector2(this);
        result.setX(result.getX() * number);
        result.setY(result.getY() * number);
        return result;
    }

    public Vector2 divide(int number) {
        Vector2 result = new Vector2(this);
        result.setX(result.getX() / number);
        result.setY(result.getY() / number);
        return result;
    }

    public boolean equals(Vector2 vector) {
        return this.x == vector.getX() && this.y == vector.getY();
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public Vector2() {
        this(0, 0);
    }

    public Vector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector2(Vector2 vector) {
        this(vector.getX(), vector.getY());
    }

    public Vector2(Vector3 vector) {
        this(vector.getX(), vector.getY());
    }
}
