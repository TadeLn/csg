package tadeln.csg;

public class Vector3 {

    private int x;

    private int y;

    private int z;

    public int getX() {
        return x;
    }

    public Vector3 setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Vector3 setY(int y) {
        this.y = y;
        return this;
    }

    public int getZ() {
        return z;
    }

    public Vector3 setZ(int z) {
        this.z = z;
        return this;
    }

    public Vector3 add(Vector3 vector) {
        Vector3 result = new Vector3(this);
        result.setX(result.getX() + vector.getX());
        result.setY(result.getY() + vector.getY());
        result.setZ(result.getZ() + vector.getZ());
        return result;
    }

    public Vector3 add(Vector2 vector) {
        Vector3 result = new Vector3(this);
        result.setX(result.getX() + vector.getX());
        result.setY(result.getY() + vector.getY());
        return result;
    }

    public Vector3 subtract(Vector3 vector) {
        Vector3 result = new Vector3(this);
        result.setX(result.getX() - vector.getX());
        result.setY(result.getY() - vector.getY());
        result.setZ(result.getZ() - vector.getZ());
        return result;
    }

    public Vector3 invert() {
        Vector3 result = new Vector3(this);
        result.setX(-result.getX());
        result.setY(-result.getY());
        result.setZ(-result.getZ());
        return result;
    }

    public Vector3 multiply(int number) {
        Vector3 result = new Vector3(this);
        result.setX(result.getX() * number);
        result.setY(result.getY() * number);
        result.setZ(result.getZ() * number);
        return result;
    }

    public Vector3 divide(int number) {
        Vector3 result = new Vector3(this);
        result.setX(result.getX() / number);
        result.setY(result.getY() / number);
        result.setZ(result.getZ() / number);
        return result;
    }

    public boolean equals(Vector3 vector) {
        return this.x == vector.getX() && this.y == vector.getY() && this.z == vector.getZ();
    }

    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

    public Vector3() {
        this(0, 0, 0);
    }

    public Vector3(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(Vector3 vector) {
        this(vector.getX(), vector.getY(), vector.getZ());
    }

    public Vector3(Vector2 vector) {
        this(vector.getX(), vector.getY(), 0);
    }
}
